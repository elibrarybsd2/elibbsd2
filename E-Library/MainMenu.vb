﻿Public Class MainMenu
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.WindowState = FormWindowState.Maximized
    End Sub

    Private Sub GANTIPASSWORDToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GANTIPASSWORDToolStripMenuItem.Click
        Password.Show()
    End Sub

    Private Sub PEGAWAIToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PEGAWAIToolStripMenuItem.Click
        Pegawai.Show()
    End Sub

    Private Sub MAHASISWAToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MAHASISWAToolStripMenuItem.Click
        Mahasiswa.Show()
    End Sub

    Private Sub BOOKToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BOOKToolStripMenuItem.Click
        Book.Show()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        LabelJam.Text = Format(Now, “H:mm:ss”)
        LabelTanggal.Text = Format(Now, “dddd, dd - MMMM - yyyy”)
    End Sub
End Class
