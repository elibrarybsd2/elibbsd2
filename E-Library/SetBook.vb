﻿Imports MySql.Data.MySqlClient
Public Class SetBook
    Private Sub SetBook_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.WindowState = FormWindowState.Maximized
        loadAll()
    End Sub
    Dim mysqlconn As MySqlConnection
    Dim mysqlcommand As MySqlCommand
    Dim SDA As New MySqlDataAdapter
    Dim dbDataSet As New DataSet
    Public Sub loadAll()
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        mysqlconn.Open()
        SDA = New MySqlDataAdapter("select * from databuku  ", mysqlconn)
        SDA.Fill(dbDataSet, "databuku")

        DataGridView1.Rows.Clear()
        DataGridView1.Columns.Clear()
        DataGridView1.Columns.Add("title", "title")
        DataGridView1.Columns.Add("author", "author")
        DataGridView1.Columns.Add("publisher", "publisher")
        DataGridView1.Columns.Add("category", "category")
        DataGridView1.Columns.Add("price", "price")
        DataGridView1.Columns.Add("downloaded", "downloaded")
        For i As Integer = 0 To dbDataSet.Tables("databuku").Rows.Count - 1
            DataGridView1.Rows.Add(
            dbDataSet.Tables("databuku").Rows(i)("title").ToString(),
            dbDataSet.Tables("databuku").Rows(i)("author").ToString(),
            dbDataSet.Tables("databuku").Rows(i)("publisher").ToString(),
            dbDataSet.Tables("databuku").Rows(i)("category").ToString(),
            dbDataSet.Tables("databuku").Rows(i)("price").ToString() + " Points",
            dbDataSet.Tables("databuku").Rows(i)("download").ToString() + " Times"
            )
        Next
        dbDataSet.Tables("databuku").Clear()

    End Sub
    Private Sub MetroTile1_Click(sender As Object, e As EventArgs) Handles MetroTile1.Click
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        Dim mysqlreader As MySqlDataReader
        Try
            mysqlconn.Open()
            Dim query As String
            query = "update tmplib.databuku set featured ='1' where title = '" & DataGridView1.Item(0, DataGridView1.CurrentRow.Index).Value & "'"
            mysqlcommand = New MySqlCommand(query, mysqlconn)
            mysqlreader = mysqlcommand.ExecuteReader
            mysqlconn.Close()
            MessageBox.Show("Data Updated")
        Catch ex As MySqlException
        Finally
            mysqlconn.Dispose()
        End Try
    End Sub

    Private Sub MetroTile2_Click(sender As Object, e As EventArgs) Handles MetroTile2.Click
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        Dim mysqlreader As MySqlDataReader
        Try
            mysqlconn.Open()
            Dim query As String
            query = "update tmplib.databuku set epick ='1' where title = '" & DataGridView1.Item(0, DataGridView1.CurrentRow.Index).Value & "'"
            mysqlcommand = New MySqlCommand(query, mysqlconn)
            mysqlreader = mysqlcommand.ExecuteReader
            mysqlconn.Close()
            MessageBox.Show("Data Updated")
        Catch ex As MySqlException
        Finally
            mysqlconn.Dispose()
        End Try
    End Sub

    Private Sub MetroTile4_Click(sender As Object, e As EventArgs) Handles MetroTile4.Click
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        Dim mysqlreader As MySqlDataReader
        Try
            mysqlconn.Open()
            Dim query As String
            query = "update tmplib.databuku set featured ='0' where title = '" & DataGridView1.Item(0, DataGridView1.CurrentRow.Index).Value & "'"
            mysqlcommand = New MySqlCommand(query, mysqlconn)
            mysqlreader = mysqlcommand.ExecuteReader
            mysqlconn.Close()
            MessageBox.Show("Data Updated")
        Catch ex As MySqlException
        Finally
            mysqlconn.Dispose()
        End Try
    End Sub

    Private Sub MetroTile3_Click(sender As Object, e As EventArgs) Handles MetroTile3.Click
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        Dim mysqlreader As MySqlDataReader
        Try
            mysqlconn.Open()
            Dim query As String
            query = "update tmplib.databuku set epick ='0' where title = '" & DataGridView1.Item(0, DataGridView1.CurrentRow.Index).Value & "'"
            mysqlcommand = New MySqlCommand(query, mysqlconn)
            mysqlreader = mysqlcommand.ExecuteReader
            mysqlconn.Close()
            MessageBox.Show("Data Updated")
        Catch ex As MySqlException
        Finally
            mysqlconn.Dispose()
        End Try
    End Sub
End Class