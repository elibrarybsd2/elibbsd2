﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CariKategori
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.MetroTextBox21 = New MetroFramework.Controls.MetroTextBox()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.MetroComboBox1 = New MetroFramework.Controls.MetroComboBox()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button12
        '
        Me.Button12.BackColor = System.Drawing.SystemColors.HotTrack
        Me.Button12.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button12.Location = New System.Drawing.Point(636, 101)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(49, 31)
        Me.Button12.TabIndex = 104
        Me.Button12.Text = "..."
        Me.Button12.UseVisualStyleBackColor = False
        '
        'MetroTextBox21
        '
        '
        '
        '
        Me.MetroTextBox21.CustomButton.Image = Nothing
        Me.MetroTextBox21.CustomButton.Location = New System.Drawing.Point(367, 1)
        Me.MetroTextBox21.CustomButton.Name = ""
        Me.MetroTextBox21.CustomButton.Size = New System.Drawing.Size(29, 29)
        Me.MetroTextBox21.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MetroTextBox21.CustomButton.TabIndex = 1
        Me.MetroTextBox21.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MetroTextBox21.CustomButton.UseSelectable = True
        Me.MetroTextBox21.CustomButton.Visible = False
        Me.MetroTextBox21.Lines = New String(-1) {}
        Me.MetroTextBox21.Location = New System.Drawing.Point(232, 101)
        Me.MetroTextBox21.MaxLength = 32767
        Me.MetroTextBox21.Multiline = True
        Me.MetroTextBox21.Name = "MetroTextBox21"
        Me.MetroTextBox21.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MetroTextBox21.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MetroTextBox21.SelectedText = ""
        Me.MetroTextBox21.SelectionLength = 0
        Me.MetroTextBox21.SelectionStart = 0
        Me.MetroTextBox21.Size = New System.Drawing.Size(397, 31)
        Me.MetroTextBox21.TabIndex = 102
        Me.MetroTextBox21.UseSelectable = True
        Me.MetroTextBox21.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MetroTextBox21.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(38, 138)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowTemplate.Height = 24
        Me.DataGridView1.Size = New System.Drawing.Size(660, 420)
        Me.DataGridView1.TabIndex = 101
        '
        'MetroComboBox1
        '
        Me.MetroComboBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroComboBox1.FormattingEnabled = True
        Me.MetroComboBox1.ItemHeight = 24
        Me.MetroComboBox1.Items.AddRange(New Object() {"Kode", "Genre"})
        Me.MetroComboBox1.Location = New System.Drawing.Point(38, 101)
        Me.MetroComboBox1.Name = "MetroComboBox1"
        Me.MetroComboBox1.Size = New System.Drawing.Size(174, 30)
        Me.MetroComboBox1.TabIndex = 107
        Me.MetroComboBox1.UseSelectable = True
        '
        'CariKategori
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(744, 610)
        Me.Controls.Add(Me.MetroComboBox1)
        Me.Controls.Add(Me.Button12)
        Me.Controls.Add(Me.MetroTextBox21)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "CariKategori"
        Me.Text = "Cari Kategori"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Button12 As Button
    Friend WithEvents MetroTextBox21 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents MetroComboBox1 As MetroFramework.Controls.MetroComboBox
End Class
