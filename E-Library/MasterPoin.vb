﻿Imports MySql.Data.MySqlClient
Public Class MasterPoin
    Dim mysqlconn As MySqlConnection
    Dim mysqlcommand As MySqlCommand
    Dim mysqlreader As MySqlDataReader
    Dim SDA As New MySqlDataAdapter
    Dim dbDataSet As New DataSet
    Private Sub MetroButton1_Click(sender As Object, e As EventArgs) Handles MetroButton1.Click
        Dim res = MessageBox.Show("Are you sure confirm this changes ?", "Warning!", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
        If res = Windows.Forms.DialogResult.Yes Then
            mysqlconn = New MySqlConnection
            mysqlconn.ConnectionString = Mastermain.koneksi
            Dim mysqlreader As MySqlDataReader
            Try
                mysqlconn.Open()
                Dim query As String
                query = "insert into tmplib.poin(id, tgl, harga ,admin) values(0,'" & adminArea.tang & "','" & MetroTextBox2.Text & "','" & adminArea.nama & "')"
                mysqlcommand = New MySqlCommand(query, mysqlconn)
                mysqlreader = mysqlcommand.ExecuteReader
                mysqlconn.Close()
                MessageBox.Show("Data Updated")
                loadRecent()
            Catch ex As MySqlException
            Finally
                mysqlconn.Dispose()
            End Try
            MessageBox.Show("Data Changed")
        Else
            MessageBox.Show("Data not Changed")
        End If

    End Sub

    Private Sub MetroButton2_Click(sender As Object, e As EventArgs) Handles MetroButton2.Click
        Dim res = MessageBox.Show("Are you sure confirm this purchase ?", "Warning!", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
        If res = Windows.Forms.DialogResult.Yes Then
            Dim tmp As String
            mysqlconn = New MySqlConnection
            mysqlconn.ConnectionString = Mastermain.koneksi
            Dim mysqlreader As MySqlDataReader
            Try
                mysqlconn.Open()
                Dim query As String
                query = "select admin from tmplib.beli where id = '" & MetroTextBox1.Text & "'"
                mysqlcommand = mysqlconn.CreateCommand()
                mysqlcommand.CommandText = query
                tmp = mysqlcommand.ExecuteScalar()
                mysqlconn.Close()
            Catch ex As MySqlException
                MessageBox.Show(ex.Message)
            Finally
                mysqlconn.Dispose()
            End Try
            If tmp = "" Then
                Try
                    mysqlconn.Open()
                    Dim query As String
                    query = "update tmplib.beli set admin ='" & adminArea.user & "',date='" & adminArea.tang & "' where id = '" & MetroTextBox1.Text & "'"
                    mysqlcommand = New MySqlCommand(query, mysqlconn)
                    mysqlreader = mysqlcommand.ExecuteReader
                    mysqlconn.Close()
                Catch ex As MySqlException
                    MessageBox.Show(ex.Message)
                Finally
                    mysqlconn.Dispose()
                End Try

            End If
            'Rp. 0
            'MetroLabel7

            Try
                mysqlconn.Open()
                Dim query As String
                query = "select ccode from tmplib.beli where id = '" & MetroTextBox1.Text & "'"
                mysqlcommand = mysqlconn.CreateCommand()
                mysqlcommand.CommandText = query
                MetroTextBox3.Text = mysqlcommand.ExecuteScalar()
                mysqlconn.Close()
            Catch ex As MySqlException
                MessageBox.Show(ex.Message)
            Finally
                mysqlconn.Dispose()
            End Try
            MessageBox.Show("Purchase confirmed!")
        Else
            MessageBox.Show("Purchase not confirmed!")
        End If


    End Sub


    Private Sub MetroTextBox1_KeyUp(sender As Object, e As KeyEventArgs) Handles MetroTextBox1.KeyUp
        Try
            mysqlconn.Open()
            Dim query As String
            query = "select poin from tmplib.beli where id = '" & MetroTextBox1.Text & "'"
            mysqlcommand = mysqlconn.CreateCommand()
            mysqlcommand.CommandText = query
            MetroLabel8.Text = mysqlcommand.ExecuteScalar()
            mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            mysqlconn.Dispose()
        End Try

        Try
            mysqlconn.Open()
            Dim query As String
            query = "select total from tmplib.beli where id = '" & MetroTextBox1.Text & "'"
            mysqlcommand = mysqlconn.CreateCommand()
            mysqlcommand.CommandText = query
            MetroLabel7.Text = "Rp. " + Convert.ToString(mysqlcommand.ExecuteScalar())
            mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            mysqlconn.Dispose()
        End Try
        Dim admin As String
        Try
            mysqlconn.Open()
            Dim query As String
            query = "select admin from tmplib.beli where id = '" & MetroTextBox1.Text & "'"
            mysqlcommand = mysqlconn.CreateCommand()
            mysqlcommand.CommandText = query
            admin = mysqlcommand.ExecuteScalar()
            mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            mysqlconn.Dispose()
        End Try
        If admin = "" And Not MetroLabel8.Text = "" Then
            MetroButton2.Visible = True
        Else
            MetroButton2.Visible = False
        End If
    End Sub
End Class