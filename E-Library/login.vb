﻿Imports MySql.Data.MySqlClient
Public Class login
    Public user As String
    Public ada As Boolean
    Public adminMode As Boolean
    Dim mysqlconn As MySqlConnection
    Dim mysqlreader As MySqlDataReader
    Dim mysqlcommand As MySqlCommand
    Private Sub login_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.WindowState = FormWindowState.Maximized
        adminMode = False
        MetroLabel3.Visible = False
        MetroLabel3.Text = "- Admin Mode -"
    End Sub

    Private Sub MetroButton1_Click(sender As Object, e As EventArgs) Handles MetroButton1.Click
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        If MetroTextBox1.Text <> "" And MetroTextBox2.Text <> "" And Not adminMode Then
            Try
                mysqlconn.Open()
                Dim query As String
                query = "select * from siswa where id = '" & MetroTextBox1.Text & "' and password='" & MetroTextBox2.Text & "'"
                mysqlcommand = New MySqlCommand(query, mysqlconn)
                mysqlreader = mysqlcommand.ExecuteReader
                While mysqlreader.Read
                    Mastermain.nama = (mysqlreader.GetString("nama"))
                    Mastermain.poin = (mysqlreader.GetInt32("poin"))
                    Mastermain.user = MetroTextBox1.Text
                    Mastermain.user = MetroTextBox1.Text
                    Mastermain.loginBang()
                    Me.Close()
                    ada = True
                End While
                mysqlconn.Close()
            Catch ex As MySqlException
                MessageBox.Show(ex.Message)
            Finally
                mysqlconn.Dispose()
            End Try
        ElseIf MetroTextBox1.Text <> "" And MetroTextBox2.Text <> "" And adminMode Then
            Try
                mysqlconn.Open()
                Dim query As String
                query = "select * from pegawai where id = '" & MetroTextBox1.Text & "' and password='" & MetroTextBox2.Text & "'"
                mysqlcommand = New MySqlCommand(query, mysqlconn)
                mysqlreader = mysqlcommand.ExecuteReader
                While mysqlreader.Read
                    adminArea.nama = (mysqlreader.GetString("nama"))
                    adminArea.password = (mysqlreader.GetString("password"))
                    adminArea.user = MetroTextBox1.Text
                    adminArea.Show()
                    Me.Close()
                    ada = True
                End While
                mysqlconn.Close()
            Catch ex As MySqlException
                MessageBox.Show(ex.Message)
            Finally
                mysqlconn.Dispose()
            End Try
        End If
        If Not ada Then
            MessageBox.Show("Data tidak ditemukan")
        Else
            ada = False
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If adminMode Then
            adminMode = False
            MetroLabel3.Visible = False
        Else
            adminMode = True
            MetroLabel3.Visible = True
        End If
    End Sub

    Private Sub MetroButton2_Click(sender As Object, e As EventArgs) Handles MetroButton2.Click
        register.Show()
    End Sub

    Private Sub MetroButton3_Click(sender As Object, e As EventArgs) Handles MetroButton3.Click
        forgotpass.Show()
    End Sub

    Private Sub MetroButton4_Click(sender As Object, e As EventArgs) Handles MetroButton4.Click
        FormServer.Show()
    End Sub
End Class