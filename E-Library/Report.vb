﻿Imports MySql.Data.MySqlClient
Public Class Report

    Dim mysqlconn As MySqlConnection
    Dim mysqlcommand As MySqlCommand
    Dim SDA As New MySqlDataAdapter
    Dim dbDataSet As New DataSet
    Dim mysqlreader As MySqlDataReader

    Private Sub Report_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        mysqlconn.Open()
        SDA = New MySqlDataAdapter("select * from beli  ", mysqlconn)
        SDA.Fill(dbDataSet, "beli")
        DataGridView1.Rows.Clear()
        DataGridView1.Columns.Clear()
        DataGridView1.Columns.Add("id", "id")
        DataGridView1.Columns.Add("poin", "poin")
        DataGridView1.Columns.Add("harga", "harga")
        DataGridView1.Columns.Add("total", "total")
        DataGridView1.Columns.Add("ccode", "ccode")
        DataGridView1.Columns.Add("admin", "admin")
        DataGridView1.Columns.Add("tgl", "date")
        For i As Integer = 0 To dbDataSet.Tables("beli").Rows.Count - 1
            DataGridView1.Rows.Add(
            dbDataSet.Tables("beli").Rows(i)("id").ToString(),
            dbDataSet.Tables("beli").Rows(i)("poin").ToString(),
            dbDataSet.Tables("beli").Rows(i)("harga").ToString(),
            dbDataSet.Tables("beli").Rows(i)("total").ToString(),
            dbDataSet.Tables("beli").Rows(i)("ccode").ToString(),
            dbDataSet.Tables("beli").Rows(i)("admin").ToString(),
            dbDataSet.Tables("beli").Rows(i)("date").ToString()
            )
        Next
        dbDataSet.Tables("beli").Clear()

        SDA = New MySqlDataAdapter("select * from poin  ", mysqlconn)
        SDA.Fill(dbDataSet, "poin")
        DataGridView2.Rows.Clear()
        DataGridView2.Columns.Clear()
        DataGridView2.Columns.Add("id", "id")
        DataGridView2.Columns.Add("tgl", "tgl")
        DataGridView2.Columns.Add("harga", "harga")
        DataGridView2.Columns.Add("admin", "admin")
        For i As Integer = 0 To dbDataSet.Tables("poin").Rows.Count - 1
            DataGridView2.Rows.Add(
            dbDataSet.Tables("poin").Rows(i)("id").ToString(),
            dbDataSet.Tables("poin").Rows(i)("tgl").ToString(),
            dbDataSet.Tables("poin").Rows(i)("harga").ToString(),
            dbDataSet.Tables("poin").Rows(i)("admin").ToString()
            )
        Next
        dbDataSet.Tables("poin").Clear()

        SDA = New MySqlDataAdapter("select * from library  ", mysqlconn)
        SDA.Fill(dbDataSet, "library")
        DataGridView3.Rows.Clear()
        DataGridView3.Columns.Clear()
        DataGridView3.Columns.Add("id", "id")
        DataGridView3.Columns.Add("idbuku", "idbuku")
        DataGridView3.Columns.Add("title", "title")
        DataGridView3.Columns.Add("buyer", "buyer")
        DataGridView3.Columns.Add("tanggal", "tanggal")
        For i As Integer = 0 To dbDataSet.Tables("library").Rows.Count - 1
            DataGridView3.Rows.Add(
            dbDataSet.Tables("library").Rows(i)("id").ToString(),
            dbDataSet.Tables("library").Rows(i)("idbuku").ToString(),
            dbDataSet.Tables("library").Rows(i)("title").ToString(),
            dbDataSet.Tables("library").Rows(i)("buyer").ToString(),
            dbDataSet.Tables("library").Rows(i)("tanggal").ToString()
            )
        Next
        dbDataSet.Tables("library").Clear()

        'SDA = New MySqlDataAdapter("select * from library  ", mysqlconn)
        'SDA.Fill(dbDataSet, "library")
        'DataGridView4.Rows.Clear()
        'DataGridView4.Columns.Clear()
        'DataGridView4.Columns.Add("id", "id")
        'DataGridView4.Columns.Add("idbuku", "idbuku")
        'DataGridView4.Columns.Add("title", "title")
        'DataGridView4.Columns.Add("buyer", "buyer")
        'DataGridView4.Columns.Add("tanggal", "tanggal")
        'For i As Integer = 0 To dbDataSet.Tables("poin").Rows.Count - 1
        '    DataGridView3.Rows.Add(
        '    dbDataSet.Tables("library").Rows(i)("id").ToString(),
        '    dbDataSet.Tables("library").Rows(i)("idbuku").ToString(),
        '    dbDataSet.Tables("library").Rows(i)("title").ToString(),
        '    dbDataSet.Tables("library").Rows(i)("buyer").ToString(),
        '    dbDataSet.Tables("library").Rows(i)("tanggal").ToString()
        '    )
        'Next
        'dbDataSet.Tables("poin").Clear()

        Me.WindowState = FormWindowState.Maximized
    End Sub
End Class