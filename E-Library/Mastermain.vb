﻿Public Class Mastermain
    Public user As String
    Public nama As String
    Public koneksi As String
    Public poin As Int32

    Private Sub main_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.WindowState = FormWindowState.Maximized
        user = ""
        nama = ""
        MetroPanel2.Hide()
    End Sub
    Public Sub buatKoneksi(data As String)
        koneksi = data
    End Sub

    Private Sub MetroButton1_Click(sender As Object, e As EventArgs) Handles MetroButton1.Click
        If MetroButton1.Text = "login" Then
            login.Show()
        Else
            login.user = ""
            user = ""
            nama = ""
            poin = 0
            logoutbang()
        End If
    End Sub
    Private Sub MetroLink1_Click(sender As Object, e As EventArgs) Handles MetroLink1.Click
        profile.Show()
    End Sub

    Private Sub MetroTile5_Click(sender As Object, e As EventArgs) Handles MetroTile5.Click

        FormMainBuku.Text = "E-Library | All Books"

        FormMainBuku.loadAll()
        FormMainBuku.Show()
    End Sub

    Private Sub MetroTile2_Click(sender As Object, e As EventArgs) Handles MetroTile2.Click
        FormMainBuku.Text = "E-Library | Last Added"
        FormMainBuku.loadLast()
        FormMainBuku.Show()

    End Sub

    Private Sub MetroTile1_Click(sender As Object, e As EventArgs) Handles MetroTile1.Click
        FormMainBuku.Text = "E-Library | Featured"
        FormMainBuku.Show()
        FormMainBuku.loadFeatured()
    End Sub

    Private Sub MetroTile3_Click(sender As Object, e As EventArgs) Handles MetroTile3.Click
        FormMainBuku.Text = "E-Library | New Release"
        FormMainBuku.LoadNew()
        FormMainBuku.Show()
    End Sub

    Private Sub MetroTile6_Click(sender As Object, e As EventArgs) Handles MetroTile6.Click
        FormMainBuku.Text = "E-Library | Most Downloaded"
        FormMainBuku.loadTopDwnl()
        FormMainBuku.Show()
    End Sub

    Public Sub loginBang()
        MetroPanel2.Show()
        MetroLink1.Text = nama
        MetroLabel2.Text = poin
        MetroButton1.Text = "logout"
        profile.resetPoin()
    End Sub
    Public Sub logoutbang()
        MetroPanel2.Hide()
        MetroLink1.Text = "Guest"
        MetroButton1.Text = "login"
    End Sub


    Private Sub MetroTile4_Click(sender As Object, e As EventArgs) Handles MetroTile4.Click
        FormMainBuku.Text = "E-Library | Editor Pick's"
        FormMainBuku.Show()
        FormMainBuku.loadEpick()
    End Sub
End Class