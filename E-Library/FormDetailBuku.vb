﻿Imports System.Drawing.Imaging
Imports MySql.Data.MySqlClient
Imports System.IO.Stream

Public Class FormDetailBuku

    Dim simpanan As String
    Dim title As String
    Dim dwn As String
    Dim tang As String

    Private Sub FormDetailBuku_MouseEnter(sender As Object, e As EventArgs) Handles MyBase.MouseEnter
        MetroTile2.Style = 6
        MetroTile3.Style = 6
    End Sub

    Private Sub MetroTile2_MouseEnter(sender As Object, e As EventArgs) Handles MetroTile2.MouseEnter
        MetroTile2.Style = 5
    End Sub
    Private Sub MetroTile3_MouseEnter(sender As Object, e As EventArgs) Handles MetroTile3.MouseEnter
        MetroTile3.Style = 5
    End Sub

    Dim mysqlconn As MySqlConnection
    Dim mysqlcommand As MySqlCommand
    Dim SDA As New MySqlDataAdapter
    Dim mysqlreader As MySqlDataReader
    Dim dbDataSet As New DataSet

    Public Sub cariBukuBos(t As String)
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        Try
            mysqlconn.Open()
            Dim query As String
            query = "select * from tmplib.databuku where  title = '" & t & "'"
            title = t
            mysqlcommand = New MySqlCommand(query, mysqlconn)
            mysqlreader = mysqlcommand.ExecuteReader
            While mysqlreader.Read
                Me.Text = "E-Library | " + t
                simpanan = mysqlreader.GetString("id")
                MetroLabel5.Text = mysqlreader.GetString("year")
                MetroTextBox1.Text = mysqlreader.GetString("description")
                Dim price As String
                If mysqlreader.GetInt32("price") = 0 Then
                    price = "Free"
                Else
                    price = mysqlreader.GetString("price")
                End If
                MetroLabel7.Text = price
                MetroLabel2.Text = mysqlreader.GetString("author")
                MetroLabel3.Text = mysqlreader.GetString("publisher")
                MetroLabel12.Text = mysqlreader.GetString("category")
                dwn = mysqlreader.GetString("download")
            End While
            mysqlconn.Close()
        Catch ex As MySqlException
            MsgBox(ex.Message)
        Finally
            mysqlconn.Dispose()
        End Try
        PictureBox1.Load(Application.StartupPath & "\data\" & simpanan & ".jpg")
        PictureBox1.SizeMode = PictureBoxSizeMode.StretchImage
        cekBuku()
    End Sub

    Public Sub cekBuku()
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        Try
            mysqlconn.Open()
            Dim query As String
            query = "select * from tmplib.library where idbuku = '" & simpanan & "' and buyer='" & Mastermain.user & "'"
            mysqlcommand = New MySqlCommand(query, mysqlconn)
            mysqlreader = mysqlcommand.ExecuteReader
            While mysqlreader.Read
                MetroTile2.Visible = False
            End While
            mysqlconn.Close()
        Catch ex As MySqlException
        Finally
            mysqlconn.Dispose()
        End Try
    End Sub

    Private Sub FormDetailBuku_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If Mastermain.user = "" Then
            MetroTile2.Visible = False
            MetroTextBox2.Visible = False
            MetroTile3.Visible = False
            MetroTrackBar1.Visible = False
            MetroLabel14.Visible = False
        End If
        loadKomen()
        loadBintang()
        loadRelated()
    End Sub

    Private Sub MetroTile2_Click(sender As Object, e As EventArgs) Handles MetroTile2.Click
        Dim res = MessageBox.Show("Are you sure to buy this book ?", "Warning!", MessageBoxButtons.YesNo, MessageBoxIcon.Information)

        If res = Windows.Forms.DialogResult.Yes Then
            If Mastermain.poin >= MetroLabel7.Text Or MetroLabel7.Text = "Free" Then
                mysqlconn = New MySqlConnection
                mysqlconn.ConnectionString = Mastermain.koneksi
                Dim mysqlreader As MySqlDataReader
                Try
                    mysqlconn.Open()
                    Dim query As String
                    query = "update siswa set poin = '" & Mastermain.poin - Integer.Parse(MetroLabel7.Text) & "' where id = '" & Mastermain.user & "'"
                    mysqlcommand = New MySqlCommand(query, mysqlconn)
                    mysqlreader = mysqlcommand.ExecuteReader
                    mysqlconn.Close()
                Catch ex As MySqlException
                Finally
                    mysqlconn.Dispose()
                End Try
                Try
                    mysqlconn.Open()
                    Dim query As String
                    query = "update databuku set download = '" & dwn + 1 & "' where id = '" & simpanan & "'"
                    mysqlcommand = New MySqlCommand(query, mysqlconn)
                    mysqlreader = mysqlcommand.ExecuteReader
                    mysqlconn.Close()
                Catch ex As MySqlException
                Finally
                    mysqlconn.Dispose()
                End Try
                Try
                    mysqlconn.Open()
                    Dim query As String
                    query = "insert into tmplib.library(id, idbuku, title, buyer, tanggal) values(0,'" & simpanan & "','" & title & "','" & Mastermain.user & "','" & tang & "')"
                    mysqlcommand = New MySqlCommand(query, mysqlconn)
                    mysqlreader = mysqlcommand.ExecuteReader
                    mysqlconn.Close()
                    MessageBox.Show("Added to Your Library!")
                Catch ex As MySqlException
                    MessageBox.Show(ex.Message)
                Finally
                    mysqlconn.Dispose()
                End Try
                cekBuku()
                Mastermain.poin = Mastermain.poin - Integer.Parse(MetroLabel7.Text)
                Mastermain.loginBang()
                FormMainBuku.loadAll()
            End If
        Else
            MessageBox.Show("Purchase Canceled!")
        End If
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        tang = Format(Now, “dddd, dd - MMMM - yyyy”)
        loadKomen()
        loadBintang()
    End Sub

    Private Sub MetroTile3_Click(sender As Object, e As EventArgs) Handles MetroTile3.Click
        Try
            mysqlconn.Open()
            Dim query As String
            query = "insert into tmplib.komen(id, nama, komentar, idbuku, tanggal) values(0,'" & Mastermain.nama & "','" & MetroTextBox2.Text & "','" & simpanan & "','" & tang & "')"
            mysqlcommand = New MySqlCommand(query, mysqlconn)
            mysqlreader = mysqlcommand.ExecuteReader
            mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            mysqlconn.Dispose()
        End Try
        loadKomen()
        MetroTextBox2.Text = ""

        Try
            mysqlconn.Open()
            Dim query As String
            query = "insert into tmplib.rating(id, rating) values('" & simpanan & "','" & Integer.Parse(MetroTrackBar1.Value) & "')"
            mysqlcommand = New MySqlCommand(query, mysqlconn)
            mysqlreader = mysqlcommand.ExecuteReader
            mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            mysqlconn.Dispose()
        End Try
        loadBintang()
    End Sub

    Private Sub loadRelated()
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        mysqlconn.Open()
        SDA = New MySqlDataAdapter("select * from databuku where category = '" & MetroLabel12.Text & "'", mysqlconn)
        SDA.Fill(dbDataSet, "databuku")

        DataGridView1.Rows.Clear()
        DataGridView1.Columns.Clear()
        DataGridView1.Columns.Add("title", "title")
        DataGridView1.Columns.Add("publisher", "publisher")
        DataGridView1.Columns.Add("price", "price")
        For i As Integer = 0 To dbDataSet.Tables("databuku").Rows.Count - 1
            If Not dbDataSet.Tables("databuku").Rows(i)("id").ToString() = simpanan Then
                DataGridView1.Rows.Add(
            dbDataSet.Tables("databuku").Rows(i)("title").ToString(),
            dbDataSet.Tables("databuku").Rows(i)("publisher").ToString(),
            dbDataSet.Tables("databuku").Rows(i)("price").ToString() + " Points"
            )
            End If

        Next
        dbDataSet.Tables("databuku").Clear()
        mysqlconn.Close()
    End Sub

    Private Sub loadKomen()
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        Try
            DataGridView2.Rows.Clear()
            DataGridView2.Columns.Clear()
            DataGridView2.Columns.Add("Name", "Name")
            DataGridView2.Columns.Add("Review", "Review")
            DataGridView2.Columns.Add("Date", "Date")
            mysqlconn.Open()
            Dim query As String
            query = "select * from tmplib.komen where idbuku='" & simpanan & "'"
            SDA = New MySqlDataAdapter(query, mysqlconn)
            SDA.Fill(dbDataSet, "komen")
            For i As Integer = 0 To dbDataSet.Tables("komen").Rows.Count - 1
                DataGridView2.Rows.Add(
            dbDataSet.Tables("komen").Rows(i)("nama").ToString(),
            dbDataSet.Tables("komen").Rows(i)("komentar").ToString(),
            dbDataSet.Tables("komen").Rows(i)("tanggal").ToString()
            )
            Next
            dbDataSet.Tables("komen").Clear()
            mysqlconn.Close()
        Catch ex As MySqlException
        Finally
            mysqlconn.Dispose()
        End Try
    End Sub

    Private Sub loadBintang()
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        Try
            mysqlconn.Open()
            Dim query As String
            query = "select * from tmplib.rating where id = '" & simpanan & "'"
            mysqlcommand = New MySqlCommand(query, mysqlconn)
            mysqlreader = mysqlcommand.ExecuteReader
            Dim counter As Int32 = 0
            Dim rating As Int32 = 0
            While mysqlreader.Read
                counter = counter + 1
                rating = rating + mysqlreader.GetInt32("rating")
            End While
            MetroLabel16.Text = Convert.ToString(rating / counter) + "/5"
            If counter = 0 Then
                MetroLabel16.Text = "0/5"
            End If
            mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            mysqlconn.Dispose()
        End Try
    End Sub

    Private Sub MetroTrackBar1_Scroll(sender As Object, e As ScrollEventArgs) Handles MetroTrackBar1.Scroll
        MetroLabel14.Text = "☺ " + Convert.ToString(MetroTrackBar1.Value)
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs)



    End Sub

    Private Function transferData(data As String) As List(Of ObjGambar)
        Dim tmp As String
        Me.Text = "E-Library | " & data
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        Try
            mysqlconn.Open()
            Dim query As String
            query = "select id from databuku where title='" & data & "'"
            mysqlcommand = mysqlconn.CreateCommand()
            mysqlcommand.CommandText = query
            tmp = mysqlcommand.ExecuteScalar()
            mysqlconn.Close()
        Catch ex As MySqlException
        Finally
            mysqlconn.Dispose()
        End Try

        Dim pdfFile As String = Application.StartupPath & "\pdf\" & tmp & ".pdf"
        Dim pdfToImg As New NReco.PdfRenderer.PdfToImageConverter()
        pdfToImg.ScaleTo = 200

        Dim previewAmount = 3
        Dim gambar123 As New List(Of ObjGambar)

        For index = 1 To previewAmount
            Dim obj As New ObjGambar(pdfToImg.GenerateImage(pdfFile, index))
            gambar123.Add(obj)
        Next

        'Dim image1 = pdfToImg.GenerateImage(pdfFile, 1)
        'Dim image2 = pdfToImg.GenerateImage(pdfFile, 2)
        'Dim image3 = pdfToImg.GenerateImage(pdfFile, 3)


        'gambar123.Add(image1)
        'gambar123.Add(image2)
        'gambar123.Add(image3)

        Return gambar123
    End Function

    Private Class ObjGambar
        Dim image As Image
        Sub New(img As Image)
            Me.image = img
        End Sub

        Function GetImg()
            Return Me.image
        End Function
    End Class

    Private Sub MetroTile1_Click(sender As Object, e As EventArgs) Handles MetroTile1.Click
        Dim pg As New PreviewGambar()
        pg.Show()

        'TabPage tp = New TabPage("Test");
        'tabControl1.TabPages.Add(tp);

        'TextBox tb = New TextBox();
        'tb.Dock = DockStyle.Fill;
        'tb.Multiline = True;

        'tp.Controls.Add(tb);

        Dim gambarToShow As List(Of ObjGambar)

        gambarToShow = transferData(DataGridView1.Item(0, DataGridView1.CurrentRow.Index).Value)

        pg.PictureBox1.Image = gambarToShow(0).GetImg
        pg.PictureBox2.Image = gambarToShow(1).GetImg
        pg.PictureBox3.Image = gambarToShow(2).GetImg
    End Sub

End Class