﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MasterBuku
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MasterBuku))
        Me.MetroTabControl1 = New MetroFramework.Controls.MetroTabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.MetroTabControl2 = New MetroFramework.Controls.MetroTabControl()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.MetroTile16 = New MetroFramework.Controls.MetroTile()
        Me.MetroTile3 = New MetroFramework.Controls.MetroTile()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.MetroTile2 = New MetroFramework.Controls.MetroTile()
        Me.MetroComboBox3 = New MetroFramework.Controls.MetroComboBox()
        Me.MetroComboBox2 = New MetroFramework.Controls.MetroComboBox()
        Me.MetroComboBox1 = New MetroFramework.Controls.MetroComboBox()
        Me.MetroTextBox7 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroTextBox6 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroTextBox4 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroTextBox1 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroTextBox8 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel8 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel7 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel6 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel4 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.MetroTabControl4 = New MetroFramework.Controls.MetroTabControl()
        Me.TabPage10 = New System.Windows.Forms.TabPage()
        Me.MetroPanel5 = New MetroFramework.Controls.MetroPanel()
        Me.MetroTile10 = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel30 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel31 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel32 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel33 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel34 = New MetroFramework.Controls.MetroLabel()
        Me.MetroRadioButton2 = New MetroFramework.Controls.MetroRadioButton()
        Me.MetroRadioButton1 = New MetroFramework.Controls.MetroRadioButton()
        Me.MetroTile11 = New MetroFramework.Controls.MetroTile()
        Me.MetroTextBox19 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroTextBox20 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroTextBox21 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroTextBox22 = New MetroFramework.Controls.MetroTextBox()
        Me.TabPage11 = New System.Windows.Forms.TabPage()
        Me.MetroPanel6 = New MetroFramework.Controls.MetroPanel()
        Me.MetroTile13 = New MetroFramework.Controls.MetroTile()
        Me.MetroTile12 = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel35 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel36 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel37 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel38 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel39 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel40 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel41 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel42 = New MetroFramework.Controls.MetroLabel()
        Me.MetroTile15 = New MetroFramework.Controls.MetroTile()
        Me.MetroTextBox23 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroTextBox24 = New MetroFramework.Controls.MetroTextBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.MetroTabControl3 = New MetroFramework.Controls.MetroTabControl()
        Me.TabPage7 = New System.Windows.Forms.TabPage()
        Me.MetroPanel3 = New MetroFramework.Controls.MetroPanel()
        Me.MetroTextBox12 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel20 = New MetroFramework.Controls.MetroLabel()
        Me.MetroTile6 = New MetroFramework.Controls.MetroTile()
        Me.MetroTile7 = New MetroFramework.Controls.MetroTile()
        Me.MetroTextBox10 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroTextBox11 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroTextBox13 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroTextBox14 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel18 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel19 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel24 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel25 = New MetroFramework.Controls.MetroLabel()
        Me.TabPage8 = New System.Windows.Forms.TabPage()
        Me.MetroPanel4 = New MetroFramework.Controls.MetroPanel()
        Me.MetroTile14 = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel28 = New MetroFramework.Controls.MetroLabel()
        Me.MetroTextBox15 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel21 = New MetroFramework.Controls.MetroLabel()
        Me.MetroTile8 = New MetroFramework.Controls.MetroTile()
        Me.MetroTile9 = New MetroFramework.Controls.MetroTile()
        Me.MetroTextBox16 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroTextBox17 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroTextBox18 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel22 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel23 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel26 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel27 = New MetroFramework.Controls.MetroLabel()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.OpenFileDialog2 = New System.Windows.Forms.OpenFileDialog()
        Me.MetroTabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.MetroTabControl2.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.MetroPanel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        Me.MetroTabControl4.SuspendLayout()
        Me.TabPage10.SuspendLayout()
        Me.MetroPanel5.SuspendLayout()
        Me.TabPage11.SuspendLayout()
        Me.MetroPanel6.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.MetroTabControl3.SuspendLayout()
        Me.TabPage7.SuspendLayout()
        Me.MetroPanel3.SuspendLayout()
        Me.TabPage8.SuspendLayout()
        Me.MetroPanel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'MetroTabControl1
        '
        Me.MetroTabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroTabControl1.Controls.Add(Me.TabPage1)
        Me.MetroTabControl1.Controls.Add(Me.TabPage3)
        Me.MetroTabControl1.Controls.Add(Me.TabPage2)
        Me.MetroTabControl1.Location = New System.Drawing.Point(23, 63)
        Me.MetroTabControl1.Name = "MetroTabControl1"
        Me.MetroTabControl1.SelectedIndex = 0
        Me.MetroTabControl1.Size = New System.Drawing.Size(920, 423)
        Me.MetroTabControl1.TabIndex = 0
        Me.MetroTabControl1.UseSelectable = True
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.MetroTabControl2)
        Me.TabPage1.Location = New System.Drawing.Point(4, 38)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(912, 381)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Books"
        '
        'MetroTabControl2
        '
        Me.MetroTabControl2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroTabControl2.Controls.Add(Me.TabPage4)
        Me.MetroTabControl2.Location = New System.Drawing.Point(0, 0)
        Me.MetroTabControl2.Name = "MetroTabControl2"
        Me.MetroTabControl2.SelectedIndex = 0
        Me.MetroTabControl2.Size = New System.Drawing.Size(916, 385)
        Me.MetroTabControl2.TabIndex = 0
        Me.MetroTabControl2.UseSelectable = True
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.MetroPanel1)
        Me.TabPage4.Location = New System.Drawing.Point(4, 38)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(908, 343)
        Me.TabPage4.TabIndex = 0
        Me.TabPage4.Text = "Insert Book"
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroPanel1.Controls.Add(Me.MetroTile16)
        Me.MetroPanel1.Controls.Add(Me.MetroTile3)
        Me.MetroPanel1.Controls.Add(Me.PictureBox1)
        Me.MetroPanel1.Controls.Add(Me.MetroTile2)
        Me.MetroPanel1.Controls.Add(Me.MetroComboBox3)
        Me.MetroPanel1.Controls.Add(Me.MetroComboBox2)
        Me.MetroPanel1.Controls.Add(Me.MetroComboBox1)
        Me.MetroPanel1.Controls.Add(Me.MetroTextBox7)
        Me.MetroPanel1.Controls.Add(Me.MetroTextBox6)
        Me.MetroPanel1.Controls.Add(Me.MetroTextBox4)
        Me.MetroPanel1.Controls.Add(Me.MetroTextBox1)
        Me.MetroPanel1.Controls.Add(Me.MetroTextBox8)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel8)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel7)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel6)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel5)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel4)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel2)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel1)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel3)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(0, 0)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(913, 353)
        Me.MetroPanel1.TabIndex = 0
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'MetroTile16
        '
        Me.MetroTile16.ActiveControl = Nothing
        Me.MetroTile16.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroTile16.Location = New System.Drawing.Point(578, 248)
        Me.MetroTile16.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTile16.Name = "MetroTile16"
        Me.MetroTile16.Size = New System.Drawing.Size(180, 29)
        Me.MetroTile16.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroTile16.TabIndex = 71
        Me.MetroTile16.Text = "Upload PDF file"
        Me.MetroTile16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile16.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile16.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular
        Me.MetroTile16.UseSelectable = True
        '
        'MetroTile3
        '
        Me.MetroTile3.ActiveControl = Nothing
        Me.MetroTile3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroTile3.Location = New System.Drawing.Point(578, 214)
        Me.MetroTile3.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTile3.Name = "MetroTile3"
        Me.MetroTile3.Size = New System.Drawing.Size(180, 29)
        Me.MetroTile3.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroTile3.TabIndex = 70
        Me.MetroTile3.Text = "Upload Image"
        Me.MetroTile3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile3.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile3.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular
        Me.MetroTile3.UseSelectable = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(578, 14)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(180, 195)
        Me.PictureBox1.TabIndex = 69
        Me.PictureBox1.TabStop = False
        '
        'MetroTile2
        '
        Me.MetroTile2.ActiveControl = Nothing
        Me.MetroTile2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroTile2.Location = New System.Drawing.Point(201, 313)
        Me.MetroTile2.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTile2.Name = "MetroTile2"
        Me.MetroTile2.Size = New System.Drawing.Size(570, 30)
        Me.MetroTile2.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroTile2.TabIndex = 68
        Me.MetroTile2.Text = "Submit"
        Me.MetroTile2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile2.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile2.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular
        Me.MetroTile2.UseSelectable = True
        '
        'MetroComboBox3
        '
        Me.MetroComboBox3.FormattingEnabled = True
        Me.MetroComboBox3.ItemHeight = 23
        Me.MetroComboBox3.Items.AddRange(New Object() {"Agriculture", "Architecture", "Art", "Biography & Memoir", "Business", "Children's Fiction", "Children's Non-Fiction", "Cooking, Food & Wine", "Crafts & Hobbies", "Family & Parenting", "Fiction", "Fun & Games", "Gardening", "Health, Mind, & Body", "History", "Home Reference / How-to", "Humor", "Medical", "Music", "Performing Arts", "Pets", "Philosophy", "Photography", "Reference", "Religion", "Self-Help", "Social Science", "Sports & Recreation", "Transportation", "Travel "})
        Me.MetroComboBox3.Location = New System.Drawing.Point(281, 170)
        Me.MetroComboBox3.Name = "MetroComboBox3"
        Me.MetroComboBox3.Size = New System.Drawing.Size(292, 29)
        Me.MetroComboBox3.TabIndex = 66
        Me.MetroComboBox3.UseSelectable = True
        '
        'MetroComboBox2
        '
        Me.MetroComboBox2.FormattingEnabled = True
        Me.MetroComboBox2.ItemHeight = 23
        Me.MetroComboBox2.Location = New System.Drawing.Point(281, 105)
        Me.MetroComboBox2.Name = "MetroComboBox2"
        Me.MetroComboBox2.Size = New System.Drawing.Size(292, 29)
        Me.MetroComboBox2.TabIndex = 65
        Me.MetroComboBox2.UseSelectable = True
        '
        'MetroComboBox1
        '
        Me.MetroComboBox1.FormattingEnabled = True
        Me.MetroComboBox1.ItemHeight = 23
        Me.MetroComboBox1.Location = New System.Drawing.Point(281, 73)
        Me.MetroComboBox1.Name = "MetroComboBox1"
        Me.MetroComboBox1.Size = New System.Drawing.Size(292, 29)
        Me.MetroComboBox1.TabIndex = 64
        Me.MetroComboBox1.UseSelectable = True
        '
        'MetroTextBox7
        '
        Me.MetroTextBox7.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.MetroTextBox7.CustomButton.Image = Nothing
        Me.MetroTextBox7.CustomButton.Location = New System.Drawing.Point(268, 1)
        Me.MetroTextBox7.CustomButton.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox7.CustomButton.Name = ""
        Me.MetroTextBox7.CustomButton.Size = New System.Drawing.Size(23, 23)
        Me.MetroTextBox7.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MetroTextBox7.CustomButton.TabIndex = 1
        Me.MetroTextBox7.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MetroTextBox7.CustomButton.UseSelectable = True
        Me.MetroTextBox7.CustomButton.Visible = False
        Me.MetroTextBox7.Lines = New String(-1) {}
        Me.MetroTextBox7.Location = New System.Drawing.Point(281, 282)
        Me.MetroTextBox7.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox7.MaxLength = 32767
        Me.MetroTextBox7.Multiline = True
        Me.MetroTextBox7.Name = "MetroTextBox7"
        Me.MetroTextBox7.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MetroTextBox7.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MetroTextBox7.SelectedText = ""
        Me.MetroTextBox7.SelectionLength = 0
        Me.MetroTextBox7.SelectionStart = 0
        Me.MetroTextBox7.Size = New System.Drawing.Size(292, 25)
        Me.MetroTextBox7.TabIndex = 63
        Me.MetroTextBox7.UseSelectable = True
        Me.MetroTextBox7.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MetroTextBox7.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroTextBox6
        '
        Me.MetroTextBox6.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.MetroTextBox6.CustomButton.Image = Nothing
        Me.MetroTextBox6.CustomButton.Location = New System.Drawing.Point(220, 1)
        Me.MetroTextBox6.CustomButton.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox6.CustomButton.Name = ""
        Me.MetroTextBox6.CustomButton.Size = New System.Drawing.Size(71, 71)
        Me.MetroTextBox6.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MetroTextBox6.CustomButton.TabIndex = 1
        Me.MetroTextBox6.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MetroTextBox6.CustomButton.UseSelectable = True
        Me.MetroTextBox6.CustomButton.Visible = False
        Me.MetroTextBox6.Lines = New String(-1) {}
        Me.MetroTextBox6.Location = New System.Drawing.Point(281, 204)
        Me.MetroTextBox6.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox6.MaxLength = 32767
        Me.MetroTextBox6.Multiline = True
        Me.MetroTextBox6.Name = "MetroTextBox6"
        Me.MetroTextBox6.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MetroTextBox6.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MetroTextBox6.SelectedText = ""
        Me.MetroTextBox6.SelectionLength = 0
        Me.MetroTextBox6.SelectionStart = 0
        Me.MetroTextBox6.Size = New System.Drawing.Size(292, 73)
        Me.MetroTextBox6.TabIndex = 62
        Me.MetroTextBox6.UseSelectable = True
        Me.MetroTextBox6.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MetroTextBox6.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroTextBox4
        '
        Me.MetroTextBox4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.MetroTextBox4.CustomButton.Image = Nothing
        Me.MetroTextBox4.CustomButton.Location = New System.Drawing.Point(268, 1)
        Me.MetroTextBox4.CustomButton.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox4.CustomButton.Name = ""
        Me.MetroTextBox4.CustomButton.Size = New System.Drawing.Size(23, 23)
        Me.MetroTextBox4.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MetroTextBox4.CustomButton.TabIndex = 1
        Me.MetroTextBox4.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MetroTextBox4.CustomButton.UseSelectable = True
        Me.MetroTextBox4.CustomButton.Visible = False
        Me.MetroTextBox4.Lines = New String(-1) {}
        Me.MetroTextBox4.Location = New System.Drawing.Point(281, 140)
        Me.MetroTextBox4.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox4.MaxLength = 32767
        Me.MetroTextBox4.Multiline = True
        Me.MetroTextBox4.Name = "MetroTextBox4"
        Me.MetroTextBox4.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MetroTextBox4.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MetroTextBox4.SelectedText = ""
        Me.MetroTextBox4.SelectionLength = 0
        Me.MetroTextBox4.SelectionStart = 0
        Me.MetroTextBox4.Size = New System.Drawing.Size(292, 25)
        Me.MetroTextBox4.TabIndex = 60
        Me.MetroTextBox4.UseSelectable = True
        Me.MetroTextBox4.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MetroTextBox4.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroTextBox1
        '
        Me.MetroTextBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.MetroTextBox1.CustomButton.Image = Nothing
        Me.MetroTextBox1.CustomButton.Location = New System.Drawing.Point(268, 1)
        Me.MetroTextBox1.CustomButton.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox1.CustomButton.Name = ""
        Me.MetroTextBox1.CustomButton.Size = New System.Drawing.Size(23, 23)
        Me.MetroTextBox1.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MetroTextBox1.CustomButton.TabIndex = 1
        Me.MetroTextBox1.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MetroTextBox1.CustomButton.UseSelectable = True
        Me.MetroTextBox1.CustomButton.Visible = False
        Me.MetroTextBox1.Lines = New String(-1) {}
        Me.MetroTextBox1.Location = New System.Drawing.Point(281, 43)
        Me.MetroTextBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox1.MaxLength = 32767
        Me.MetroTextBox1.Multiline = True
        Me.MetroTextBox1.Name = "MetroTextBox1"
        Me.MetroTextBox1.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MetroTextBox1.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MetroTextBox1.SelectedText = ""
        Me.MetroTextBox1.SelectionLength = 0
        Me.MetroTextBox1.SelectionStart = 0
        Me.MetroTextBox1.Size = New System.Drawing.Size(292, 25)
        Me.MetroTextBox1.TabIndex = 57
        Me.MetroTextBox1.UseSelectable = True
        Me.MetroTextBox1.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MetroTextBox1.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroTextBox8
        '
        Me.MetroTextBox8.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.MetroTextBox8.CustomButton.Image = Nothing
        Me.MetroTextBox8.CustomButton.Location = New System.Drawing.Point(268, 1)
        Me.MetroTextBox8.CustomButton.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox8.CustomButton.Name = ""
        Me.MetroTextBox8.CustomButton.Size = New System.Drawing.Size(23, 23)
        Me.MetroTextBox8.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MetroTextBox8.CustomButton.TabIndex = 1
        Me.MetroTextBox8.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MetroTextBox8.CustomButton.UseSelectable = True
        Me.MetroTextBox8.CustomButton.Visible = False
        Me.MetroTextBox8.Lines = New String(-1) {}
        Me.MetroTextBox8.Location = New System.Drawing.Point(281, 14)
        Me.MetroTextBox8.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox8.MaxLength = 32767
        Me.MetroTextBox8.Multiline = True
        Me.MetroTextBox8.Name = "MetroTextBox8"
        Me.MetroTextBox8.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MetroTextBox8.ReadOnly = True
        Me.MetroTextBox8.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MetroTextBox8.SelectedText = ""
        Me.MetroTextBox8.SelectionLength = 0
        Me.MetroTextBox8.SelectionStart = 0
        Me.MetroTextBox8.Size = New System.Drawing.Size(292, 25)
        Me.MetroTextBox8.TabIndex = 56
        Me.MetroTextBox8.UseSelectable = True
        Me.MetroTextBox8.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MetroTextBox8.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel8
        '
        Me.MetroLabel8.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel8.AutoSize = True
        Me.MetroLabel8.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel8.Location = New System.Drawing.Point(201, 288)
        Me.MetroLabel8.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel8.Name = "MetroLabel8"
        Me.MetroLabel8.Size = New System.Drawing.Size(38, 19)
        Me.MetroLabel8.TabIndex = 55
        Me.MetroLabel8.Text = "Price"
        '
        'MetroLabel7
        '
        Me.MetroLabel7.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel7.AutoSize = True
        Me.MetroLabel7.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel7.Location = New System.Drawing.Point(201, 204)
        Me.MetroLabel7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel7.Name = "MetroLabel7"
        Me.MetroLabel7.Size = New System.Drawing.Size(78, 19)
        Me.MetroLabel7.TabIndex = 54
        Me.MetroLabel7.Text = "Description"
        '
        'MetroLabel6
        '
        Me.MetroLabel6.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel6.AutoSize = True
        Me.MetroLabel6.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel6.Location = New System.Drawing.Point(201, 180)
        Me.MetroLabel6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel6.Name = "MetroLabel6"
        Me.MetroLabel6.Size = New System.Drawing.Size(65, 19)
        Me.MetroLabel6.TabIndex = 53
        Me.MetroLabel6.Text = "Category"
        '
        'MetroLabel5
        '
        Me.MetroLabel5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel5.Location = New System.Drawing.Point(201, 146)
        Me.MetroLabel5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(35, 19)
        Me.MetroLabel5.TabIndex = 52
        Me.MetroLabel5.Text = "Year"
        '
        'MetroLabel4
        '
        Me.MetroLabel4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel4.AutoSize = True
        Me.MetroLabel4.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel4.Location = New System.Drawing.Point(201, 115)
        Me.MetroLabel4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel4.Name = "MetroLabel4"
        Me.MetroLabel4.Size = New System.Drawing.Size(65, 19)
        Me.MetroLabel4.TabIndex = 51
        Me.MetroLabel4.Text = "Publisher"
        '
        'MetroLabel2
        '
        Me.MetroLabel2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel2.Location = New System.Drawing.Point(201, 83)
        Me.MetroLabel2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(52, 19)
        Me.MetroLabel2.TabIndex = 50
        Me.MetroLabel2.Text = "Author"
        '
        'MetroLabel1
        '
        Me.MetroLabel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel1.Location = New System.Drawing.Point(201, 49)
        Me.MetroLabel1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(34, 19)
        Me.MetroLabel1.TabIndex = 49
        Me.MetroLabel1.Text = "Title"
        '
        'MetroLabel3
        '
        Me.MetroLabel3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel3.Location = New System.Drawing.Point(201, 20)
        Me.MetroLabel3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(23, 19)
        Me.MetroLabel3.TabIndex = 48
        Me.MetroLabel3.Text = "ID"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.MetroTabControl4)
        Me.TabPage3.Location = New System.Drawing.Point(4, 38)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(912, 381)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Author"
        '
        'MetroTabControl4
        '
        Me.MetroTabControl4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroTabControl4.Controls.Add(Me.TabPage10)
        Me.MetroTabControl4.Controls.Add(Me.TabPage11)
        Me.MetroTabControl4.Location = New System.Drawing.Point(0, 0)
        Me.MetroTabControl4.Name = "MetroTabControl4"
        Me.MetroTabControl4.SelectedIndex = 0
        Me.MetroTabControl4.Size = New System.Drawing.Size(916, 385)
        Me.MetroTabControl4.TabIndex = 2
        Me.MetroTabControl4.UseSelectable = True
        '
        'TabPage10
        '
        Me.TabPage10.Controls.Add(Me.MetroPanel5)
        Me.TabPage10.Location = New System.Drawing.Point(4, 38)
        Me.TabPage10.Name = "TabPage10"
        Me.TabPage10.Size = New System.Drawing.Size(908, 343)
        Me.TabPage10.TabIndex = 0
        Me.TabPage10.Text = "Insert Author"
        '
        'MetroPanel5
        '
        Me.MetroPanel5.Controls.Add(Me.MetroTile10)
        Me.MetroPanel5.Controls.Add(Me.MetroLabel30)
        Me.MetroPanel5.Controls.Add(Me.MetroLabel31)
        Me.MetroPanel5.Controls.Add(Me.MetroLabel32)
        Me.MetroPanel5.Controls.Add(Me.MetroLabel33)
        Me.MetroPanel5.Controls.Add(Me.MetroLabel34)
        Me.MetroPanel5.Controls.Add(Me.MetroRadioButton2)
        Me.MetroPanel5.Controls.Add(Me.MetroRadioButton1)
        Me.MetroPanel5.Controls.Add(Me.MetroTile11)
        Me.MetroPanel5.Controls.Add(Me.MetroTextBox19)
        Me.MetroPanel5.Controls.Add(Me.MetroTextBox20)
        Me.MetroPanel5.Controls.Add(Me.MetroTextBox21)
        Me.MetroPanel5.Controls.Add(Me.MetroTextBox22)
        Me.MetroPanel5.HorizontalScrollbarBarColor = True
        Me.MetroPanel5.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel5.HorizontalScrollbarSize = 10
        Me.MetroPanel5.Location = New System.Drawing.Point(0, 0)
        Me.MetroPanel5.Name = "MetroPanel5"
        Me.MetroPanel5.Size = New System.Drawing.Size(920, 385)
        Me.MetroPanel5.TabIndex = 1
        Me.MetroPanel5.VerticalScrollbarBarColor = True
        Me.MetroPanel5.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel5.VerticalScrollbarSize = 10
        '
        'MetroTile10
        '
        Me.MetroTile10.ActiveControl = Nothing
        Me.MetroTile10.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroTile10.Location = New System.Drawing.Point(162, 301)
        Me.MetroTile10.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTile10.Name = "MetroTile10"
        Me.MetroTile10.Size = New System.Drawing.Size(574, 30)
        Me.MetroTile10.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroTile10.TabIndex = 69
        Me.MetroTile10.Text = "Submit"
        Me.MetroTile10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile10.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile10.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular
        Me.MetroTile10.UseSelectable = True
        '
        'MetroLabel30
        '
        Me.MetroLabel30.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel30.AutoSize = True
        Me.MetroLabel30.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel30.Location = New System.Drawing.Point(162, 231)
        Me.MetroLabel30.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel30.Name = "MetroLabel30"
        Me.MetroLabel30.Size = New System.Drawing.Size(48, 19)
        Me.MetroLabel30.TabIndex = 47
        Me.MetroLabel30.Text = "Phone"
        '
        'MetroLabel31
        '
        Me.MetroLabel31.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel31.AutoSize = True
        Me.MetroLabel31.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel31.Location = New System.Drawing.Point(162, 167)
        Me.MetroLabel31.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel31.Name = "MetroLabel31"
        Me.MetroLabel31.Size = New System.Drawing.Size(58, 19)
        Me.MetroLabel31.TabIndex = 46
        Me.MetroLabel31.Text = "Address"
        '
        'MetroLabel32
        '
        Me.MetroLabel32.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel32.AutoSize = True
        Me.MetroLabel32.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel32.Location = New System.Drawing.Point(162, 131)
        Me.MetroLabel32.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel32.Name = "MetroLabel32"
        Me.MetroLabel32.Size = New System.Drawing.Size(54, 19)
        Me.MetroLabel32.TabIndex = 45
        Me.MetroLabel32.Text = "Gender"
        '
        'MetroLabel33
        '
        Me.MetroLabel33.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel33.AutoSize = True
        Me.MetroLabel33.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel33.Location = New System.Drawing.Point(162, 97)
        Me.MetroLabel33.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel33.Name = "MetroLabel33"
        Me.MetroLabel33.Size = New System.Drawing.Size(45, 19)
        Me.MetroLabel33.TabIndex = 44
        Me.MetroLabel33.Text = "Name"
        '
        'MetroLabel34
        '
        Me.MetroLabel34.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel34.AutoSize = True
        Me.MetroLabel34.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel34.Location = New System.Drawing.Point(162, 67)
        Me.MetroLabel34.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel34.Name = "MetroLabel34"
        Me.MetroLabel34.Size = New System.Drawing.Size(23, 19)
        Me.MetroLabel34.TabIndex = 43
        Me.MetroLabel34.Text = "ID"
        '
        'MetroRadioButton2
        '
        Me.MetroRadioButton2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroRadioButton2.AutoSize = True
        Me.MetroRadioButton2.Location = New System.Drawing.Point(425, 135)
        Me.MetroRadioButton2.Name = "MetroRadioButton2"
        Me.MetroRadioButton2.Size = New System.Drawing.Size(61, 15)
        Me.MetroRadioButton2.TabIndex = 42
        Me.MetroRadioButton2.Text = "Female"
        Me.MetroRadioButton2.UseSelectable = True
        '
        'MetroRadioButton1
        '
        Me.MetroRadioButton1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroRadioButton1.AutoSize = True
        Me.MetroRadioButton1.Location = New System.Drawing.Point(275, 135)
        Me.MetroRadioButton1.Name = "MetroRadioButton1"
        Me.MetroRadioButton1.Size = New System.Drawing.Size(49, 15)
        Me.MetroRadioButton1.TabIndex = 41
        Me.MetroRadioButton1.Text = "Male"
        Me.MetroRadioButton1.UseSelectable = True
        '
        'MetroTile11
        '
        Me.MetroTile11.ActiveControl = Nothing
        Me.MetroTile11.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroTile11.Location = New System.Drawing.Point(556, 61)
        Me.MetroTile11.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTile11.Name = "MetroTile11"
        Me.MetroTile11.Size = New System.Drawing.Size(180, 195)
        Me.MetroTile11.TabIndex = 40
        Me.MetroTile11.Text = "Upload Pic"
        Me.MetroTile11.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.MetroTile11.TileImage = CType(resources.GetObject("MetroTile11.TileImage"), System.Drawing.Image)
        Me.MetroTile11.TileImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.MetroTile11.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold
        Me.MetroTile11.UseSelectable = True
        Me.MetroTile11.UseTileImage = True
        '
        'MetroTextBox19
        '
        Me.MetroTextBox19.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.MetroTextBox19.CustomButton.Image = Nothing
        Me.MetroTextBox19.CustomButton.Location = New System.Drawing.Point(268, 1)
        Me.MetroTextBox19.CustomButton.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox19.CustomButton.Name = ""
        Me.MetroTextBox19.CustomButton.Size = New System.Drawing.Size(23, 23)
        Me.MetroTextBox19.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MetroTextBox19.CustomButton.TabIndex = 1
        Me.MetroTextBox19.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MetroTextBox19.CustomButton.UseSelectable = True
        Me.MetroTextBox19.CustomButton.Visible = False
        Me.MetroTextBox19.Lines = New String(-1) {}
        Me.MetroTextBox19.Location = New System.Drawing.Point(260, 231)
        Me.MetroTextBox19.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox19.MaxLength = 32767
        Me.MetroTextBox19.Multiline = True
        Me.MetroTextBox19.Name = "MetroTextBox19"
        Me.MetroTextBox19.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MetroTextBox19.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MetroTextBox19.SelectedText = ""
        Me.MetroTextBox19.SelectionLength = 0
        Me.MetroTextBox19.SelectionStart = 0
        Me.MetroTextBox19.Size = New System.Drawing.Size(292, 25)
        Me.MetroTextBox19.TabIndex = 36
        Me.MetroTextBox19.UseSelectable = True
        Me.MetroTextBox19.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MetroTextBox19.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroTextBox20
        '
        Me.MetroTextBox20.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.MetroTextBox20.CustomButton.Image = Nothing
        Me.MetroTextBox20.CustomButton.Location = New System.Drawing.Point(234, 2)
        Me.MetroTextBox20.CustomButton.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox20.CustomButton.Name = ""
        Me.MetroTextBox20.CustomButton.Size = New System.Drawing.Size(55, 55)
        Me.MetroTextBox20.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MetroTextBox20.CustomButton.TabIndex = 1
        Me.MetroTextBox20.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MetroTextBox20.CustomButton.UseSelectable = True
        Me.MetroTextBox20.CustomButton.Visible = False
        Me.MetroTextBox20.Lines = New String(-1) {}
        Me.MetroTextBox20.Location = New System.Drawing.Point(260, 167)
        Me.MetroTextBox20.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox20.MaxLength = 32767
        Me.MetroTextBox20.Multiline = True
        Me.MetroTextBox20.Name = "MetroTextBox20"
        Me.MetroTextBox20.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MetroTextBox20.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MetroTextBox20.SelectedText = ""
        Me.MetroTextBox20.SelectionLength = 0
        Me.MetroTextBox20.SelectionStart = 0
        Me.MetroTextBox20.Size = New System.Drawing.Size(292, 60)
        Me.MetroTextBox20.TabIndex = 37
        Me.MetroTextBox20.UseSelectable = True
        Me.MetroTextBox20.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MetroTextBox20.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroTextBox21
        '
        Me.MetroTextBox21.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.MetroTextBox21.CustomButton.Image = Nothing
        Me.MetroTextBox21.CustomButton.Location = New System.Drawing.Point(268, 1)
        Me.MetroTextBox21.CustomButton.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox21.CustomButton.Name = ""
        Me.MetroTextBox21.CustomButton.Size = New System.Drawing.Size(23, 23)
        Me.MetroTextBox21.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MetroTextBox21.CustomButton.TabIndex = 1
        Me.MetroTextBox21.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MetroTextBox21.CustomButton.UseSelectable = True
        Me.MetroTextBox21.CustomButton.Visible = False
        Me.MetroTextBox21.Lines = New String(-1) {}
        Me.MetroTextBox21.Location = New System.Drawing.Point(260, 91)
        Me.MetroTextBox21.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox21.MaxLength = 32767
        Me.MetroTextBox21.Multiline = True
        Me.MetroTextBox21.Name = "MetroTextBox21"
        Me.MetroTextBox21.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MetroTextBox21.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MetroTextBox21.SelectedText = ""
        Me.MetroTextBox21.SelectionLength = 0
        Me.MetroTextBox21.SelectionStart = 0
        Me.MetroTextBox21.Size = New System.Drawing.Size(292, 25)
        Me.MetroTextBox21.TabIndex = 38
        Me.MetroTextBox21.UseSelectable = True
        Me.MetroTextBox21.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MetroTextBox21.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroTextBox22
        '
        Me.MetroTextBox22.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.MetroTextBox22.CustomButton.Image = Nothing
        Me.MetroTextBox22.CustomButton.Location = New System.Drawing.Point(268, 1)
        Me.MetroTextBox22.CustomButton.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox22.CustomButton.Name = ""
        Me.MetroTextBox22.CustomButton.Size = New System.Drawing.Size(23, 23)
        Me.MetroTextBox22.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MetroTextBox22.CustomButton.TabIndex = 1
        Me.MetroTextBox22.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MetroTextBox22.CustomButton.UseSelectable = True
        Me.MetroTextBox22.CustomButton.Visible = False
        Me.MetroTextBox22.Lines = New String(-1) {}
        Me.MetroTextBox22.Location = New System.Drawing.Point(260, 61)
        Me.MetroTextBox22.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox22.MaxLength = 32767
        Me.MetroTextBox22.Multiline = True
        Me.MetroTextBox22.Name = "MetroTextBox22"
        Me.MetroTextBox22.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MetroTextBox22.ReadOnly = True
        Me.MetroTextBox22.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MetroTextBox22.SelectedText = ""
        Me.MetroTextBox22.SelectionLength = 0
        Me.MetroTextBox22.SelectionStart = 0
        Me.MetroTextBox22.Size = New System.Drawing.Size(292, 25)
        Me.MetroTextBox22.TabIndex = 39
        Me.MetroTextBox22.UseSelectable = True
        Me.MetroTextBox22.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MetroTextBox22.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'TabPage11
        '
        Me.TabPage11.Controls.Add(Me.MetroPanel6)
        Me.TabPage11.Location = New System.Drawing.Point(4, 38)
        Me.TabPage11.Name = "TabPage11"
        Me.TabPage11.Size = New System.Drawing.Size(908, 343)
        Me.TabPage11.TabIndex = 1
        Me.TabPage11.Text = "Update Author"
        '
        'MetroPanel6
        '
        Me.MetroPanel6.Controls.Add(Me.MetroTile13)
        Me.MetroPanel6.Controls.Add(Me.MetroTile12)
        Me.MetroPanel6.Controls.Add(Me.MetroLabel35)
        Me.MetroPanel6.Controls.Add(Me.MetroLabel36)
        Me.MetroPanel6.Controls.Add(Me.MetroLabel37)
        Me.MetroPanel6.Controls.Add(Me.MetroLabel38)
        Me.MetroPanel6.Controls.Add(Me.MetroLabel39)
        Me.MetroPanel6.Controls.Add(Me.MetroLabel40)
        Me.MetroPanel6.Controls.Add(Me.MetroLabel41)
        Me.MetroPanel6.Controls.Add(Me.MetroLabel42)
        Me.MetroPanel6.Controls.Add(Me.MetroTile15)
        Me.MetroPanel6.Controls.Add(Me.MetroTextBox23)
        Me.MetroPanel6.Controls.Add(Me.MetroTextBox24)
        Me.MetroPanel6.HorizontalScrollbarBarColor = True
        Me.MetroPanel6.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel6.HorizontalScrollbarSize = 10
        Me.MetroPanel6.Location = New System.Drawing.Point(0, 0)
        Me.MetroPanel6.Name = "MetroPanel6"
        Me.MetroPanel6.Size = New System.Drawing.Size(920, 385)
        Me.MetroPanel6.TabIndex = 2
        Me.MetroPanel6.VerticalScrollbarBarColor = True
        Me.MetroPanel6.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel6.VerticalScrollbarSize = 10
        '
        'MetroTile13
        '
        Me.MetroTile13.ActiveControl = Nothing
        Me.MetroTile13.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroTile13.Location = New System.Drawing.Point(162, 301)
        Me.MetroTile13.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTile13.Name = "MetroTile13"
        Me.MetroTile13.Size = New System.Drawing.Size(574, 30)
        Me.MetroTile13.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroTile13.TabIndex = 70
        Me.MetroTile13.Text = "Submit"
        Me.MetroTile13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile13.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile13.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular
        Me.MetroTile13.UseSelectable = True
        '
        'MetroTile12
        '
        Me.MetroTile12.ActiveControl = Nothing
        Me.MetroTile12.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroTile12.Location = New System.Drawing.Point(162, 21)
        Me.MetroTile12.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTile12.Name = "MetroTile12"
        Me.MetroTile12.Size = New System.Drawing.Size(155, 30)
        Me.MetroTile12.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroTile12.TabIndex = 53
        Me.MetroTile12.Text = "Search"
        Me.MetroTile12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile12.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile12.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular
        Me.MetroTile12.UseSelectable = True
        '
        'MetroLabel35
        '
        Me.MetroLabel35.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel35.AutoSize = True
        Me.MetroLabel35.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel35.Location = New System.Drawing.Point(261, 67)
        Me.MetroLabel35.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel35.Name = "MetroLabel35"
        Me.MetroLabel35.Size = New System.Drawing.Size(56, 19)
        Me.MetroLabel35.TabIndex = 52
        Me.MetroLabel35.Text = "ID Data"
        '
        'MetroLabel36
        '
        Me.MetroLabel36.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel36.AutoSize = True
        Me.MetroLabel36.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel36.Location = New System.Drawing.Point(261, 97)
        Me.MetroLabel36.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel36.Name = "MetroLabel36"
        Me.MetroLabel36.Size = New System.Drawing.Size(78, 19)
        Me.MetroLabel36.TabIndex = 51
        Me.MetroLabel36.Text = "Name Data"
        '
        'MetroLabel37
        '
        Me.MetroLabel37.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel37.AutoSize = True
        Me.MetroLabel37.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel37.Location = New System.Drawing.Point(260, 131)
        Me.MetroLabel37.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel37.Name = "MetroLabel37"
        Me.MetroLabel37.Size = New System.Drawing.Size(87, 19)
        Me.MetroLabel37.TabIndex = 50
        Me.MetroLabel37.Text = "Gender Data"
        '
        'MetroLabel38
        '
        Me.MetroLabel38.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel38.AutoSize = True
        Me.MetroLabel38.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel38.Location = New System.Drawing.Point(162, 231)
        Me.MetroLabel38.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel38.Name = "MetroLabel38"
        Me.MetroLabel38.Size = New System.Drawing.Size(48, 19)
        Me.MetroLabel38.TabIndex = 47
        Me.MetroLabel38.Text = "Phone"
        '
        'MetroLabel39
        '
        Me.MetroLabel39.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel39.AutoSize = True
        Me.MetroLabel39.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel39.Location = New System.Drawing.Point(162, 167)
        Me.MetroLabel39.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel39.Name = "MetroLabel39"
        Me.MetroLabel39.Size = New System.Drawing.Size(58, 19)
        Me.MetroLabel39.TabIndex = 46
        Me.MetroLabel39.Text = "Address"
        '
        'MetroLabel40
        '
        Me.MetroLabel40.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel40.AutoSize = True
        Me.MetroLabel40.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel40.Location = New System.Drawing.Point(162, 131)
        Me.MetroLabel40.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel40.Name = "MetroLabel40"
        Me.MetroLabel40.Size = New System.Drawing.Size(54, 19)
        Me.MetroLabel40.TabIndex = 45
        Me.MetroLabel40.Text = "Gender"
        '
        'MetroLabel41
        '
        Me.MetroLabel41.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel41.AutoSize = True
        Me.MetroLabel41.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel41.Location = New System.Drawing.Point(162, 97)
        Me.MetroLabel41.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel41.Name = "MetroLabel41"
        Me.MetroLabel41.Size = New System.Drawing.Size(45, 19)
        Me.MetroLabel41.TabIndex = 44
        Me.MetroLabel41.Text = "Name"
        '
        'MetroLabel42
        '
        Me.MetroLabel42.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel42.AutoSize = True
        Me.MetroLabel42.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel42.Location = New System.Drawing.Point(162, 67)
        Me.MetroLabel42.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel42.Name = "MetroLabel42"
        Me.MetroLabel42.Size = New System.Drawing.Size(23, 19)
        Me.MetroLabel42.TabIndex = 43
        Me.MetroLabel42.Text = "ID"
        '
        'MetroTile15
        '
        Me.MetroTile15.ActiveControl = Nothing
        Me.MetroTile15.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroTile15.Location = New System.Drawing.Point(556, 61)
        Me.MetroTile15.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTile15.Name = "MetroTile15"
        Me.MetroTile15.Size = New System.Drawing.Size(180, 195)
        Me.MetroTile15.TabIndex = 40
        Me.MetroTile15.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.MetroTile15.TileImage = CType(resources.GetObject("MetroTile15.TileImage"), System.Drawing.Image)
        Me.MetroTile15.TileImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.MetroTile15.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold
        Me.MetroTile15.UseSelectable = True
        Me.MetroTile15.UseTileImage = True
        '
        'MetroTextBox23
        '
        Me.MetroTextBox23.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.MetroTextBox23.CustomButton.Image = Nothing
        Me.MetroTextBox23.CustomButton.Location = New System.Drawing.Point(268, 1)
        Me.MetroTextBox23.CustomButton.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox23.CustomButton.Name = ""
        Me.MetroTextBox23.CustomButton.Size = New System.Drawing.Size(23, 23)
        Me.MetroTextBox23.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MetroTextBox23.CustomButton.TabIndex = 1
        Me.MetroTextBox23.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MetroTextBox23.CustomButton.UseSelectable = True
        Me.MetroTextBox23.CustomButton.Visible = False
        Me.MetroTextBox23.Lines = New String(-1) {}
        Me.MetroTextBox23.Location = New System.Drawing.Point(260, 231)
        Me.MetroTextBox23.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox23.MaxLength = 32767
        Me.MetroTextBox23.Multiline = True
        Me.MetroTextBox23.Name = "MetroTextBox23"
        Me.MetroTextBox23.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MetroTextBox23.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MetroTextBox23.SelectedText = ""
        Me.MetroTextBox23.SelectionLength = 0
        Me.MetroTextBox23.SelectionStart = 0
        Me.MetroTextBox23.Size = New System.Drawing.Size(292, 25)
        Me.MetroTextBox23.TabIndex = 36
        Me.MetroTextBox23.UseSelectable = True
        Me.MetroTextBox23.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MetroTextBox23.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroTextBox24
        '
        Me.MetroTextBox24.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.MetroTextBox24.CustomButton.Image = Nothing
        Me.MetroTextBox24.CustomButton.Location = New System.Drawing.Point(234, 2)
        Me.MetroTextBox24.CustomButton.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox24.CustomButton.Name = ""
        Me.MetroTextBox24.CustomButton.Size = New System.Drawing.Size(55, 55)
        Me.MetroTextBox24.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MetroTextBox24.CustomButton.TabIndex = 1
        Me.MetroTextBox24.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MetroTextBox24.CustomButton.UseSelectable = True
        Me.MetroTextBox24.CustomButton.Visible = False
        Me.MetroTextBox24.Lines = New String(-1) {}
        Me.MetroTextBox24.Location = New System.Drawing.Point(260, 167)
        Me.MetroTextBox24.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox24.MaxLength = 32767
        Me.MetroTextBox24.Multiline = True
        Me.MetroTextBox24.Name = "MetroTextBox24"
        Me.MetroTextBox24.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MetroTextBox24.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MetroTextBox24.SelectedText = ""
        Me.MetroTextBox24.SelectionLength = 0
        Me.MetroTextBox24.SelectionStart = 0
        Me.MetroTextBox24.Size = New System.Drawing.Size(292, 60)
        Me.MetroTextBox24.TabIndex = 37
        Me.MetroTextBox24.UseSelectable = True
        Me.MetroTextBox24.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MetroTextBox24.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.MetroTabControl3)
        Me.TabPage2.Location = New System.Drawing.Point(4, 38)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(912, 381)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Publisher"
        '
        'MetroTabControl3
        '
        Me.MetroTabControl3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroTabControl3.Controls.Add(Me.TabPage7)
        Me.MetroTabControl3.Controls.Add(Me.TabPage8)
        Me.MetroTabControl3.Location = New System.Drawing.Point(0, 0)
        Me.MetroTabControl3.Name = "MetroTabControl3"
        Me.MetroTabControl3.SelectedIndex = 1
        Me.MetroTabControl3.Size = New System.Drawing.Size(916, 385)
        Me.MetroTabControl3.TabIndex = 1
        Me.MetroTabControl3.UseSelectable = True
        '
        'TabPage7
        '
        Me.TabPage7.Controls.Add(Me.MetroPanel3)
        Me.TabPage7.Location = New System.Drawing.Point(4, 38)
        Me.TabPage7.Name = "TabPage7"
        Me.TabPage7.Size = New System.Drawing.Size(908, 343)
        Me.TabPage7.TabIndex = 0
        Me.TabPage7.Text = "Insert Publisher"
        '
        'MetroPanel3
        '
        Me.MetroPanel3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroPanel3.Controls.Add(Me.MetroTextBox12)
        Me.MetroPanel3.Controls.Add(Me.MetroLabel20)
        Me.MetroPanel3.Controls.Add(Me.MetroTile6)
        Me.MetroPanel3.Controls.Add(Me.MetroTile7)
        Me.MetroPanel3.Controls.Add(Me.MetroTextBox10)
        Me.MetroPanel3.Controls.Add(Me.MetroTextBox11)
        Me.MetroPanel3.Controls.Add(Me.MetroTextBox13)
        Me.MetroPanel3.Controls.Add(Me.MetroTextBox14)
        Me.MetroPanel3.Controls.Add(Me.MetroLabel18)
        Me.MetroPanel3.Controls.Add(Me.MetroLabel19)
        Me.MetroPanel3.Controls.Add(Me.MetroLabel24)
        Me.MetroPanel3.Controls.Add(Me.MetroLabel25)
        Me.MetroPanel3.HorizontalScrollbarBarColor = True
        Me.MetroPanel3.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel3.HorizontalScrollbarSize = 10
        Me.MetroPanel3.Location = New System.Drawing.Point(0, 0)
        Me.MetroPanel3.Name = "MetroPanel3"
        Me.MetroPanel3.Size = New System.Drawing.Size(913, 353)
        Me.MetroPanel3.TabIndex = 1
        Me.MetroPanel3.VerticalScrollbarBarColor = True
        Me.MetroPanel3.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel3.VerticalScrollbarSize = 10
        '
        'MetroTextBox12
        '
        Me.MetroTextBox12.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.MetroTextBox12.CustomButton.Image = Nothing
        Me.MetroTextBox12.CustomButton.Location = New System.Drawing.Point(268, 1)
        Me.MetroTextBox12.CustomButton.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox12.CustomButton.Name = ""
        Me.MetroTextBox12.CustomButton.Size = New System.Drawing.Size(23, 23)
        Me.MetroTextBox12.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MetroTextBox12.CustomButton.TabIndex = 1
        Me.MetroTextBox12.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MetroTextBox12.CustomButton.UseSelectable = True
        Me.MetroTextBox12.CustomButton.Visible = False
        Me.MetroTextBox12.Lines = New String(-1) {}
        Me.MetroTextBox12.Location = New System.Drawing.Point(268, 101)
        Me.MetroTextBox12.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox12.MaxLength = 32767
        Me.MetroTextBox12.Multiline = True
        Me.MetroTextBox12.Name = "MetroTextBox12"
        Me.MetroTextBox12.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MetroTextBox12.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MetroTextBox12.SelectedText = ""
        Me.MetroTextBox12.SelectionLength = 0
        Me.MetroTextBox12.SelectionStart = 0
        Me.MetroTextBox12.Size = New System.Drawing.Size(292, 25)
        Me.MetroTextBox12.TabIndex = 70
        Me.MetroTextBox12.UseSelectable = True
        Me.MetroTextBox12.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MetroTextBox12.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel20
        '
        Me.MetroLabel20.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel20.AutoSize = True
        Me.MetroLabel20.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel20.Location = New System.Drawing.Point(188, 107)
        Me.MetroLabel20.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel20.Name = "MetroLabel20"
        Me.MetroLabel20.Size = New System.Drawing.Size(50, 19)
        Me.MetroLabel20.TabIndex = 69
        Me.MetroLabel20.Text = "Owner"
        '
        'MetroTile6
        '
        Me.MetroTile6.ActiveControl = Nothing
        Me.MetroTile6.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroTile6.Location = New System.Drawing.Point(188, 257)
        Me.MetroTile6.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTile6.Name = "MetroTile6"
        Me.MetroTile6.Size = New System.Drawing.Size(570, 30)
        Me.MetroTile6.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroTile6.TabIndex = 68
        Me.MetroTile6.Text = "Submit"
        Me.MetroTile6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile6.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile6.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular
        Me.MetroTile6.UseSelectable = True
        '
        'MetroTile7
        '
        Me.MetroTile7.ActiveControl = Nothing
        Me.MetroTile7.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroTile7.Location = New System.Drawing.Point(578, 43)
        Me.MetroTile7.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTile7.Name = "MetroTile7"
        Me.MetroTile7.Size = New System.Drawing.Size(180, 195)
        Me.MetroTile7.TabIndex = 67
        Me.MetroTile7.Text = "Upload Pic"
        Me.MetroTile7.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.MetroTile7.TileImage = CType(resources.GetObject("MetroTile7.TileImage"), System.Drawing.Image)
        Me.MetroTile7.TileImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.MetroTile7.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold
        Me.MetroTile7.UseSelectable = True
        Me.MetroTile7.UseTileImage = True
        '
        'MetroTextBox10
        '
        Me.MetroTextBox10.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.MetroTextBox10.CustomButton.Image = Nothing
        Me.MetroTextBox10.CustomButton.Location = New System.Drawing.Point(268, 1)
        Me.MetroTextBox10.CustomButton.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox10.CustomButton.Name = ""
        Me.MetroTextBox10.CustomButton.Size = New System.Drawing.Size(23, 23)
        Me.MetroTextBox10.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MetroTextBox10.CustomButton.TabIndex = 1
        Me.MetroTextBox10.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MetroTextBox10.CustomButton.UseSelectable = True
        Me.MetroTextBox10.CustomButton.Visible = False
        Me.MetroTextBox10.Lines = New String(-1) {}
        Me.MetroTextBox10.Location = New System.Drawing.Point(268, 213)
        Me.MetroTextBox10.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox10.MaxLength = 32767
        Me.MetroTextBox10.Multiline = True
        Me.MetroTextBox10.Name = "MetroTextBox10"
        Me.MetroTextBox10.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MetroTextBox10.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MetroTextBox10.SelectedText = ""
        Me.MetroTextBox10.SelectionLength = 0
        Me.MetroTextBox10.SelectionStart = 0
        Me.MetroTextBox10.Size = New System.Drawing.Size(292, 25)
        Me.MetroTextBox10.TabIndex = 63
        Me.MetroTextBox10.UseSelectable = True
        Me.MetroTextBox10.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MetroTextBox10.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroTextBox11
        '
        Me.MetroTextBox11.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.MetroTextBox11.CustomButton.Image = Nothing
        Me.MetroTextBox11.CustomButton.Location = New System.Drawing.Point(214, 1)
        Me.MetroTextBox11.CustomButton.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox11.CustomButton.Name = ""
        Me.MetroTextBox11.CustomButton.Size = New System.Drawing.Size(77, 77)
        Me.MetroTextBox11.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MetroTextBox11.CustomButton.TabIndex = 1
        Me.MetroTextBox11.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MetroTextBox11.CustomButton.UseSelectable = True
        Me.MetroTextBox11.CustomButton.Visible = False
        Me.MetroTextBox11.Lines = New String(-1) {}
        Me.MetroTextBox11.Location = New System.Drawing.Point(268, 130)
        Me.MetroTextBox11.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox11.MaxLength = 32767
        Me.MetroTextBox11.Multiline = True
        Me.MetroTextBox11.Name = "MetroTextBox11"
        Me.MetroTextBox11.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MetroTextBox11.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MetroTextBox11.SelectedText = ""
        Me.MetroTextBox11.SelectionLength = 0
        Me.MetroTextBox11.SelectionStart = 0
        Me.MetroTextBox11.Size = New System.Drawing.Size(292, 79)
        Me.MetroTextBox11.TabIndex = 62
        Me.MetroTextBox11.UseSelectable = True
        Me.MetroTextBox11.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MetroTextBox11.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroTextBox13
        '
        Me.MetroTextBox13.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.MetroTextBox13.CustomButton.Image = Nothing
        Me.MetroTextBox13.CustomButton.Location = New System.Drawing.Point(268, 1)
        Me.MetroTextBox13.CustomButton.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox13.CustomButton.Name = ""
        Me.MetroTextBox13.CustomButton.Size = New System.Drawing.Size(23, 23)
        Me.MetroTextBox13.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MetroTextBox13.CustomButton.TabIndex = 1
        Me.MetroTextBox13.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MetroTextBox13.CustomButton.UseSelectable = True
        Me.MetroTextBox13.CustomButton.Visible = False
        Me.MetroTextBox13.Lines = New String(-1) {}
        Me.MetroTextBox13.Location = New System.Drawing.Point(268, 72)
        Me.MetroTextBox13.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox13.MaxLength = 32767
        Me.MetroTextBox13.Multiline = True
        Me.MetroTextBox13.Name = "MetroTextBox13"
        Me.MetroTextBox13.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MetroTextBox13.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MetroTextBox13.SelectedText = ""
        Me.MetroTextBox13.SelectionLength = 0
        Me.MetroTextBox13.SelectionStart = 0
        Me.MetroTextBox13.Size = New System.Drawing.Size(292, 25)
        Me.MetroTextBox13.TabIndex = 57
        Me.MetroTextBox13.UseSelectable = True
        Me.MetroTextBox13.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MetroTextBox13.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroTextBox14
        '
        Me.MetroTextBox14.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.MetroTextBox14.CustomButton.Image = Nothing
        Me.MetroTextBox14.CustomButton.Location = New System.Drawing.Point(268, 1)
        Me.MetroTextBox14.CustomButton.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox14.CustomButton.Name = ""
        Me.MetroTextBox14.CustomButton.Size = New System.Drawing.Size(23, 23)
        Me.MetroTextBox14.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MetroTextBox14.CustomButton.TabIndex = 1
        Me.MetroTextBox14.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MetroTextBox14.CustomButton.UseSelectable = True
        Me.MetroTextBox14.CustomButton.Visible = False
        Me.MetroTextBox14.Lines = New String(-1) {}
        Me.MetroTextBox14.Location = New System.Drawing.Point(268, 43)
        Me.MetroTextBox14.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox14.MaxLength = 32767
        Me.MetroTextBox14.Multiline = True
        Me.MetroTextBox14.Name = "MetroTextBox14"
        Me.MetroTextBox14.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MetroTextBox14.ReadOnly = True
        Me.MetroTextBox14.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MetroTextBox14.SelectedText = ""
        Me.MetroTextBox14.SelectionLength = 0
        Me.MetroTextBox14.SelectionStart = 0
        Me.MetroTextBox14.Size = New System.Drawing.Size(292, 25)
        Me.MetroTextBox14.TabIndex = 56
        Me.MetroTextBox14.UseSelectable = True
        Me.MetroTextBox14.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MetroTextBox14.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel18
        '
        Me.MetroLabel18.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel18.AutoSize = True
        Me.MetroLabel18.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel18.Location = New System.Drawing.Point(188, 219)
        Me.MetroLabel18.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel18.Name = "MetroLabel18"
        Me.MetroLabel18.Size = New System.Drawing.Size(48, 19)
        Me.MetroLabel18.TabIndex = 55
        Me.MetroLabel18.Text = "Phone"
        '
        'MetroLabel19
        '
        Me.MetroLabel19.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel19.AutoSize = True
        Me.MetroLabel19.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel19.Location = New System.Drawing.Point(188, 130)
        Me.MetroLabel19.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel19.Name = "MetroLabel19"
        Me.MetroLabel19.Size = New System.Drawing.Size(58, 19)
        Me.MetroLabel19.TabIndex = 54
        Me.MetroLabel19.Text = "Address"
        '
        'MetroLabel24
        '
        Me.MetroLabel24.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel24.AutoSize = True
        Me.MetroLabel24.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel24.Location = New System.Drawing.Point(188, 78)
        Me.MetroLabel24.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel24.Name = "MetroLabel24"
        Me.MetroLabel24.Size = New System.Drawing.Size(45, 19)
        Me.MetroLabel24.TabIndex = 49
        Me.MetroLabel24.Text = "Name"
        '
        'MetroLabel25
        '
        Me.MetroLabel25.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel25.AutoSize = True
        Me.MetroLabel25.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel25.Location = New System.Drawing.Point(188, 49)
        Me.MetroLabel25.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel25.Name = "MetroLabel25"
        Me.MetroLabel25.Size = New System.Drawing.Size(23, 19)
        Me.MetroLabel25.TabIndex = 48
        Me.MetroLabel25.Text = "ID"
        '
        'TabPage8
        '
        Me.TabPage8.Controls.Add(Me.MetroPanel4)
        Me.TabPage8.Location = New System.Drawing.Point(4, 38)
        Me.TabPage8.Name = "TabPage8"
        Me.TabPage8.Size = New System.Drawing.Size(908, 343)
        Me.TabPage8.TabIndex = 1
        Me.TabPage8.Text = "Update Publisher"
        '
        'MetroPanel4
        '
        Me.MetroPanel4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroPanel4.Controls.Add(Me.MetroTile14)
        Me.MetroPanel4.Controls.Add(Me.MetroLabel28)
        Me.MetroPanel4.Controls.Add(Me.MetroTextBox15)
        Me.MetroPanel4.Controls.Add(Me.MetroLabel21)
        Me.MetroPanel4.Controls.Add(Me.MetroTile8)
        Me.MetroPanel4.Controls.Add(Me.MetroTile9)
        Me.MetroPanel4.Controls.Add(Me.MetroTextBox16)
        Me.MetroPanel4.Controls.Add(Me.MetroTextBox17)
        Me.MetroPanel4.Controls.Add(Me.MetroTextBox18)
        Me.MetroPanel4.Controls.Add(Me.MetroLabel22)
        Me.MetroPanel4.Controls.Add(Me.MetroLabel23)
        Me.MetroPanel4.Controls.Add(Me.MetroLabel26)
        Me.MetroPanel4.Controls.Add(Me.MetroLabel27)
        Me.MetroPanel4.HorizontalScrollbarBarColor = True
        Me.MetroPanel4.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel4.HorizontalScrollbarSize = 10
        Me.MetroPanel4.Location = New System.Drawing.Point(0, 0)
        Me.MetroPanel4.Name = "MetroPanel4"
        Me.MetroPanel4.Size = New System.Drawing.Size(913, 353)
        Me.MetroPanel4.TabIndex = 2
        Me.MetroPanel4.VerticalScrollbarBarColor = True
        Me.MetroPanel4.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel4.VerticalScrollbarSize = 10
        '
        'MetroTile14
        '
        Me.MetroTile14.ActiveControl = Nothing
        Me.MetroTile14.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroTile14.Location = New System.Drawing.Point(498, 43)
        Me.MetroTile14.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTile14.Name = "MetroTile14"
        Me.MetroTile14.Size = New System.Drawing.Size(62, 25)
        Me.MetroTile14.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroTile14.TabIndex = 72
        Me.MetroTile14.Text = "Search"
        Me.MetroTile14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile14.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular
        Me.MetroTile14.UseSelectable = True
        '
        'MetroLabel28
        '
        Me.MetroLabel28.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel28.AutoSize = True
        Me.MetroLabel28.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel28.Location = New System.Drawing.Point(268, 49)
        Me.MetroLabel28.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel28.Name = "MetroLabel28"
        Me.MetroLabel28.Size = New System.Drawing.Size(23, 19)
        Me.MetroLabel28.TabIndex = 71
        Me.MetroLabel28.Text = "ID"
        '
        'MetroTextBox15
        '
        Me.MetroTextBox15.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.MetroTextBox15.CustomButton.Image = Nothing
        Me.MetroTextBox15.CustomButton.Location = New System.Drawing.Point(268, 1)
        Me.MetroTextBox15.CustomButton.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox15.CustomButton.Name = ""
        Me.MetroTextBox15.CustomButton.Size = New System.Drawing.Size(23, 23)
        Me.MetroTextBox15.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MetroTextBox15.CustomButton.TabIndex = 1
        Me.MetroTextBox15.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MetroTextBox15.CustomButton.UseSelectable = True
        Me.MetroTextBox15.CustomButton.Visible = False
        Me.MetroTextBox15.Lines = New String(-1) {}
        Me.MetroTextBox15.Location = New System.Drawing.Point(268, 101)
        Me.MetroTextBox15.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox15.MaxLength = 32767
        Me.MetroTextBox15.Multiline = True
        Me.MetroTextBox15.Name = "MetroTextBox15"
        Me.MetroTextBox15.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MetroTextBox15.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MetroTextBox15.SelectedText = ""
        Me.MetroTextBox15.SelectionLength = 0
        Me.MetroTextBox15.SelectionStart = 0
        Me.MetroTextBox15.Size = New System.Drawing.Size(292, 25)
        Me.MetroTextBox15.TabIndex = 70
        Me.MetroTextBox15.UseSelectable = True
        Me.MetroTextBox15.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MetroTextBox15.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel21
        '
        Me.MetroLabel21.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel21.AutoSize = True
        Me.MetroLabel21.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel21.Location = New System.Drawing.Point(188, 107)
        Me.MetroLabel21.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel21.Name = "MetroLabel21"
        Me.MetroLabel21.Size = New System.Drawing.Size(50, 19)
        Me.MetroLabel21.TabIndex = 69
        Me.MetroLabel21.Text = "Owner"
        '
        'MetroTile8
        '
        Me.MetroTile8.ActiveControl = Nothing
        Me.MetroTile8.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroTile8.Location = New System.Drawing.Point(188, 257)
        Me.MetroTile8.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTile8.Name = "MetroTile8"
        Me.MetroTile8.Size = New System.Drawing.Size(570, 30)
        Me.MetroTile8.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroTile8.TabIndex = 68
        Me.MetroTile8.Text = "Submit"
        Me.MetroTile8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile8.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile8.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular
        Me.MetroTile8.UseSelectable = True
        '
        'MetroTile9
        '
        Me.MetroTile9.ActiveControl = Nothing
        Me.MetroTile9.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroTile9.Location = New System.Drawing.Point(578, 43)
        Me.MetroTile9.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTile9.Name = "MetroTile9"
        Me.MetroTile9.Size = New System.Drawing.Size(180, 195)
        Me.MetroTile9.TabIndex = 67
        Me.MetroTile9.Text = "Upload Pic"
        Me.MetroTile9.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.MetroTile9.TileImage = CType(resources.GetObject("MetroTile9.TileImage"), System.Drawing.Image)
        Me.MetroTile9.TileImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.MetroTile9.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold
        Me.MetroTile9.UseSelectable = True
        Me.MetroTile9.UseTileImage = True
        '
        'MetroTextBox16
        '
        Me.MetroTextBox16.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.MetroTextBox16.CustomButton.Image = Nothing
        Me.MetroTextBox16.CustomButton.Location = New System.Drawing.Point(268, 1)
        Me.MetroTextBox16.CustomButton.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox16.CustomButton.Name = ""
        Me.MetroTextBox16.CustomButton.Size = New System.Drawing.Size(23, 23)
        Me.MetroTextBox16.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MetroTextBox16.CustomButton.TabIndex = 1
        Me.MetroTextBox16.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MetroTextBox16.CustomButton.UseSelectable = True
        Me.MetroTextBox16.CustomButton.Visible = False
        Me.MetroTextBox16.Lines = New String(-1) {}
        Me.MetroTextBox16.Location = New System.Drawing.Point(268, 213)
        Me.MetroTextBox16.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox16.MaxLength = 32767
        Me.MetroTextBox16.Multiline = True
        Me.MetroTextBox16.Name = "MetroTextBox16"
        Me.MetroTextBox16.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MetroTextBox16.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MetroTextBox16.SelectedText = ""
        Me.MetroTextBox16.SelectionLength = 0
        Me.MetroTextBox16.SelectionStart = 0
        Me.MetroTextBox16.Size = New System.Drawing.Size(292, 25)
        Me.MetroTextBox16.TabIndex = 63
        Me.MetroTextBox16.UseSelectable = True
        Me.MetroTextBox16.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MetroTextBox16.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroTextBox17
        '
        Me.MetroTextBox17.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.MetroTextBox17.CustomButton.Image = Nothing
        Me.MetroTextBox17.CustomButton.Location = New System.Drawing.Point(214, 1)
        Me.MetroTextBox17.CustomButton.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox17.CustomButton.Name = ""
        Me.MetroTextBox17.CustomButton.Size = New System.Drawing.Size(77, 77)
        Me.MetroTextBox17.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MetroTextBox17.CustomButton.TabIndex = 1
        Me.MetroTextBox17.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MetroTextBox17.CustomButton.UseSelectable = True
        Me.MetroTextBox17.CustomButton.Visible = False
        Me.MetroTextBox17.Lines = New String(-1) {}
        Me.MetroTextBox17.Location = New System.Drawing.Point(268, 130)
        Me.MetroTextBox17.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox17.MaxLength = 32767
        Me.MetroTextBox17.Multiline = True
        Me.MetroTextBox17.Name = "MetroTextBox17"
        Me.MetroTextBox17.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MetroTextBox17.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MetroTextBox17.SelectedText = ""
        Me.MetroTextBox17.SelectionLength = 0
        Me.MetroTextBox17.SelectionStart = 0
        Me.MetroTextBox17.Size = New System.Drawing.Size(292, 79)
        Me.MetroTextBox17.TabIndex = 62
        Me.MetroTextBox17.UseSelectable = True
        Me.MetroTextBox17.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MetroTextBox17.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroTextBox18
        '
        Me.MetroTextBox18.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.MetroTextBox18.CustomButton.Image = Nothing
        Me.MetroTextBox18.CustomButton.Location = New System.Drawing.Point(268, 1)
        Me.MetroTextBox18.CustomButton.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox18.CustomButton.Name = ""
        Me.MetroTextBox18.CustomButton.Size = New System.Drawing.Size(23, 23)
        Me.MetroTextBox18.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MetroTextBox18.CustomButton.TabIndex = 1
        Me.MetroTextBox18.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MetroTextBox18.CustomButton.UseSelectable = True
        Me.MetroTextBox18.CustomButton.Visible = False
        Me.MetroTextBox18.Lines = New String(-1) {}
        Me.MetroTextBox18.Location = New System.Drawing.Point(268, 72)
        Me.MetroTextBox18.Margin = New System.Windows.Forms.Padding(2)
        Me.MetroTextBox18.MaxLength = 32767
        Me.MetroTextBox18.Multiline = True
        Me.MetroTextBox18.Name = "MetroTextBox18"
        Me.MetroTextBox18.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MetroTextBox18.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MetroTextBox18.SelectedText = ""
        Me.MetroTextBox18.SelectionLength = 0
        Me.MetroTextBox18.SelectionStart = 0
        Me.MetroTextBox18.Size = New System.Drawing.Size(292, 25)
        Me.MetroTextBox18.TabIndex = 57
        Me.MetroTextBox18.UseSelectable = True
        Me.MetroTextBox18.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MetroTextBox18.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel22
        '
        Me.MetroLabel22.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel22.AutoSize = True
        Me.MetroLabel22.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel22.Location = New System.Drawing.Point(188, 219)
        Me.MetroLabel22.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel22.Name = "MetroLabel22"
        Me.MetroLabel22.Size = New System.Drawing.Size(48, 19)
        Me.MetroLabel22.TabIndex = 55
        Me.MetroLabel22.Text = "Phone"
        '
        'MetroLabel23
        '
        Me.MetroLabel23.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel23.AutoSize = True
        Me.MetroLabel23.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel23.Location = New System.Drawing.Point(188, 130)
        Me.MetroLabel23.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel23.Name = "MetroLabel23"
        Me.MetroLabel23.Size = New System.Drawing.Size(58, 19)
        Me.MetroLabel23.TabIndex = 54
        Me.MetroLabel23.Text = "Address"
        '
        'MetroLabel26
        '
        Me.MetroLabel26.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel26.AutoSize = True
        Me.MetroLabel26.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel26.Location = New System.Drawing.Point(188, 78)
        Me.MetroLabel26.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel26.Name = "MetroLabel26"
        Me.MetroLabel26.Size = New System.Drawing.Size(45, 19)
        Me.MetroLabel26.TabIndex = 49
        Me.MetroLabel26.Text = "Name"
        '
        'MetroLabel27
        '
        Me.MetroLabel27.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel27.AutoSize = True
        Me.MetroLabel27.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel27.Location = New System.Drawing.Point(188, 49)
        Me.MetroLabel27.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel27.Name = "MetroLabel27"
        Me.MetroLabel27.Size = New System.Drawing.Size(23, 19)
        Me.MetroLabel27.TabIndex = 48
        Me.MetroLabel27.Text = "ID"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'OpenFileDialog2
        '
        Me.OpenFileDialog2.FileName = "OpenFileDialog2"
        '
        'MasterBuku
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(966, 546)
        Me.Controls.Add(Me.MetroTabControl1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "MasterBuku"
        Me.Resizable = False
        Me.ShowInTaskbar = False
        Me.Text = "E-Library |  Books"
        Me.TopMost = True
        Me.MetroTabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.MetroTabControl2.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.MetroTabControl4.ResumeLayout(False)
        Me.TabPage10.ResumeLayout(False)
        Me.MetroPanel5.ResumeLayout(False)
        Me.MetroPanel5.PerformLayout()
        Me.TabPage11.ResumeLayout(False)
        Me.MetroPanel6.ResumeLayout(False)
        Me.MetroPanel6.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.MetroTabControl3.ResumeLayout(False)
        Me.TabPage7.ResumeLayout(False)
        Me.MetroPanel3.ResumeLayout(False)
        Me.MetroPanel3.PerformLayout()
        Me.TabPage8.ResumeLayout(False)
        Me.MetroPanel4.ResumeLayout(False)
        Me.MetroPanel4.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents MetroTabControl1 As MetroFramework.Controls.MetroTabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents MetroTabControl2 As MetroFramework.Controls.MetroTabControl
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents MetroTabControl3 As MetroFramework.Controls.MetroTabControl
    Friend WithEvents TabPage7 As TabPage
    Friend WithEvents TabPage8 As TabPage
    Friend WithEvents MetroTabControl4 As MetroFramework.Controls.MetroTabControl
    Friend WithEvents TabPage10 As TabPage
    Friend WithEvents TabPage11 As TabPage
    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents MetroLabel7 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel6 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel4 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel8 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroTextBox7 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroTextBox6 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroTextBox4 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroTextBox1 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroTextBox8 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroComboBox3 As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroComboBox2 As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroComboBox1 As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroTile2 As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroPanel3 As MetroFramework.Controls.MetroPanel
    Friend WithEvents MetroTile6 As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile7 As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTextBox10 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroTextBox11 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroTextBox13 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroTextBox14 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel18 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel19 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel24 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel25 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroTextBox12 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel20 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroPanel4 As MetroFramework.Controls.MetroPanel
    Friend WithEvents MetroTile14 As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel28 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroTextBox15 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel21 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroTile8 As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile9 As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTextBox16 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroTextBox17 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroTextBox18 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel22 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel23 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel26 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel27 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroPanel5 As MetroFramework.Controls.MetroPanel
    Friend WithEvents MetroLabel30 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel31 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel32 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel33 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel34 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroRadioButton2 As MetroFramework.Controls.MetroRadioButton
    Friend WithEvents MetroRadioButton1 As MetroFramework.Controls.MetroRadioButton
    Friend WithEvents MetroTile11 As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTextBox19 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroTextBox20 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroTextBox21 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroTextBox22 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroPanel6 As MetroFramework.Controls.MetroPanel
    Friend WithEvents MetroTile12 As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel35 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel36 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel37 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel38 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel39 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel40 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel41 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel42 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroTile15 As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTextBox23 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroTextBox24 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroTile10 As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile13 As MetroFramework.Controls.MetroTile
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents MetroTile3 As MetroFramework.Controls.MetroTile
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents MetroTile16 As MetroFramework.Controls.MetroTile
    Friend WithEvents OpenFileDialog2 As OpenFileDialog
End Class
