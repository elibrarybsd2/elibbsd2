﻿Imports System.Text
Imports MySql.Data.MySqlClient
Public Class tambahPoin
    Dim mysqlconn As MySqlConnection
    Dim mysqlcommand As MySqlCommand
    Dim mysqlreader As MySqlDataReader
    Dim harga As String
    Private Function rnd()
        Dim validchars As String = "asdfghjkqwertyuiopzxcvbnm1234567890ZXCVBNMASDFGHJKLQWERTYUOP"
        Dim sb As New StringBuilder()
        Dim ran As New Random()
        For i As Integer = 1 To 5
            Dim idx As Integer = ran.Next(0, validchars.Length)
            Dim randomChar As Char = validchars(idx)
            sb.Append(randomChar)
        Next i
        Return (sb.ToString())
    End Function

    Private Sub tambahPoin_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        loadData()
    End Sub
    Private Sub loadData()
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        Try
            mysqlconn.Open()
            Dim query As String
            query = "select harga from poin where id=(select max(id) from poin)"
            mysqlcommand = mysqlconn.CreateCommand()
            mysqlcommand.CommandText = query
            harga = mysqlcommand.ExecuteScalar()
            MetroLabel5.Text = "! You'll need to pay Rp." + harga + " for each poin !"
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            mysqlconn.Dispose()
        End Try
        Try
            mysqlconn.Open()
            Dim query As String
            query = "select AUTO_INCREMENT from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA = 'tmplib' and TABLE_NAME = 'beli'"
            mysqlcommand = mysqlconn.CreateCommand()
            mysqlcommand.CommandText = query
            MetroTextBox3.Text = mysqlcommand.ExecuteScalar()
            mysqlconn.Close()
        Catch ex As MySqlException
        Finally
            mysqlconn.Dispose()
        End Try
    End Sub

    Private Sub MetroButton1_Click_1(sender As Object, e As EventArgs) Handles MetroButton1.Click
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        Try
            mysqlconn.Open()
            Dim query As String
            query = "insert into tmplib.beli(
                     id,poin,harga,total,
                     ccode,admin,date,c) values(
                     0,'" & MetroTextBox2.Text & "','" & harga & "','" & MetroTextBox2.Text * harga & "',
                     '" & rnd() & "','','','" & False & "')"
            mysqlcommand = New MySqlCommand(query, mysqlconn)
            mysqlreader = mysqlcommand.ExecuteReader
            mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            mysqlconn.Dispose()
        End Try
        loadData()
        MetroTextBox2.Text = ""
    End Sub
    Public Sub resetPoin()
        Try
            mysqlconn.Open()
            Dim query As String
            query = "select * from tmplib.siswa where id = '" & Mastermain.user & "'"
            mysqlcommand = New MySqlCommand(query, mysqlconn)
            mysqlreader = mysqlcommand.ExecuteReader
            While mysqlreader.Read
                Mastermain.nama = (mysqlreader.GetString("nama"))
                Mastermain.poin = (mysqlreader.GetInt32("poin"))
            End While
            Mastermain.loginBang()
            Me.Close()
            mysqlconn.Close()
        Catch ex As MySqlException
        Finally
            mysqlconn.Dispose()
        End Try
    End Sub

    Private Sub MetroButton2_Click(sender As Object, e As EventArgs) Handles MetroButton2.Click
        Dim tmp As Boolean
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        Dim mysqlreader As MySqlDataReader
        Try
            mysqlconn.Open()
            Dim query As String
            query = "select c from tmplib.beli where ccode = '" & MetroTextBox1.Text & "'"
            mysqlcommand = mysqlconn.CreateCommand()
            mysqlcommand.CommandText = query
            tmp = mysqlcommand.ExecuteScalar()
            mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            mysqlconn.Dispose()
        End Try
        If tmp = False Then
            Dim poin As Int32
            Dim poinLm As Int32

            Try
                mysqlconn.Open()
                Dim query As String
                query = "update tmplib.beli set c = true where ccode = '" & MetroTextBox1.Text & "'"
                mysqlcommand = New MySqlCommand(query, mysqlconn)
                mysqlreader = mysqlcommand.ExecuteReader
                mysqlconn.Close()
            Catch ex As MySqlException
                MessageBox.Show(ex.Message)
            Finally
                mysqlconn.Dispose()
            End Try
            Try
                mysqlconn.Open()
                Dim query As String
                query = "select poin from tmplib.beli where ccode = '" & MetroTextBox1.Text & "'"
                mysqlcommand = mysqlconn.CreateCommand()
                mysqlcommand.CommandText = query
                poin = mysqlcommand.ExecuteScalar()
                mysqlconn.Close()
            Catch ex As MySqlException
                MessageBox.Show(ex.Message)
            Finally
                mysqlconn.Dispose()
            End Try

            Try
                mysqlconn.Open()
                Dim query As String
                query = "select poin from siswa where id = '" & Mastermain.user & "'"
                mysqlcommand = mysqlconn.CreateCommand()
                mysqlcommand.CommandText = query
                poinLm = mysqlcommand.ExecuteScalar()
                mysqlconn.Close()
            Catch ex As MySqlException
                MessageBox.Show(ex.Message)
            Finally
                mysqlconn.Dispose()
            End Try
            Try
                mysqlconn.Open()
                Dim query As String
                query = "update tmplib.siswa set poin ='" & (poinLm + poin) & "' where id = '" & Mastermain.user & "'"
                mysqlcommand = New MySqlCommand(query, mysqlconn)
                mysqlreader = mysqlcommand.ExecuteReader
                mysqlconn.Close()
            Catch ex As MySqlException
                MessageBox.Show(ex.Message)
            Finally
                mysqlconn.Dispose()
            End Try
            MetroTextBox1.Text = ""
            resetPoin()
        End If
    End Sub
End Class