﻿Public Class adminArea
    Public nama As String
    Public password As String
    Public user As String
    Public tang As String
    Public jama As String

    Private Sub adminArea_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.WindowState = FormWindowState.Maximized
        MetroLink1.Text = nama
    End Sub
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        MetroLabel2.Text = Format(Now, “H:mm:ss”)
        MetroLabel1.Text = Format(Now, “dddd, dd - MMMM - yyyy”)
        tang = MetroLabel1.Text
        jama = MetroLabel2.Text
    End Sub
    Private Sub MetroButton1_Click(sender As Object, e As EventArgs) Handles MetroButton1.Click
        login.Show()
        login.user = ""
        nama = ""
        password = ""
        user = ""
        Me.Close()
    End Sub

    Private Sub MetroTile2_Click(sender As Object, e As EventArgs) Handles MetroTile2.Click
        MasterBuku.Show()
        Me.Enabled = False
    End Sub

    Private Sub MetroTile3_Click(sender As Object, e As EventArgs) Handles MetroTile3.Click
        MasterPegawai.Show()
        Me.Enabled = False
    End Sub

    Private Sub MetroTile4_Click(sender As Object, e As EventArgs) Handles MetroTile4.Click
        SetBook.Text = "E - Library |  Set Editor Pick's"
        SetBook.Show()
    End Sub

    Private Sub MetroTile1_Click(sender As Object, e As EventArgs) Handles MetroTile1.Click
        SetBook.Text = "E - Library |  Set Featured"
        SetBook.Show()
    End Sub
    Public Sub Aktif() Handles MetroTile1.Click
        Me.Enabled = True
    End Sub

    Private Sub MetroTile6_Click(sender As Object, e As EventArgs) Handles MetroTile6.Click
        MasterPoin.Show()
    End Sub

    Private Sub MetroTile5_Click(sender As Object, e As EventArgs) Handles MetroTile5.Click
        Report.Show()
    End Sub
End Class