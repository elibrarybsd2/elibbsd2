﻿Imports MySql.Data.MySqlClient
Public Class FormCari
    Dim from As String
    Private Sub FormCari_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
    Dim mysqlconn As MySqlConnection
    Dim mysqlcommand As MySqlCommand
    Dim SDA As New MySqlDataAdapter
    Dim dbDataSet As New DataSet
    Dim mysqlreader As MySqlDataReader
    Private Sub loadTablePegawai()
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        mysqlconn.Open()
        SDA = New MySqlDataAdapter("select * from pegawai  ", mysqlconn)
        SDA.Fill(dbDataSet, "pegawai")

        DataGridView1.Rows.Clear()
        DataGridView1.Columns.Clear()
        DataGridView1.Columns.Add("Id", "id")
        DataGridView1.Columns.Add("nama", "nama")
        For i As Integer = 0 To dbDataSet.Tables("pegawai").Rows.Count - 1
            DataGridView1.Rows.Add(
            dbDataSet.Tables("pegawai").Rows(i)("id").ToString(),
            dbDataSet.Tables("pegawai").Rows(i)("nama").ToString()
            )
        Next
        dbDataSet.Tables("pegawai").Clear()
        mysqlconn.Close()
        MetroTile1.Visible = False
    End Sub

    Private Sub loadTablePublisher()
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        mysqlconn.Open()
        SDA = New MySqlDataAdapter("select * from publisher  ", mysqlconn)
        SDA.Fill(dbDataSet, "publisher")

        DataGridView1.Rows.Clear()
        DataGridView1.Columns.Clear()
        DataGridView1.Columns.Add("Id", "id")
        DataGridView1.Columns.Add("nama", "nama")
        For i As Integer = 0 To dbDataSet.Tables("publisher").Rows.Count - 1
            DataGridView1.Rows.Add(
            dbDataSet.Tables("publisher").Rows(i)("id").ToString(),
            dbDataSet.Tables("publisher").Rows(i)("nama").ToString()
            )
        Next
        dbDataSet.Tables("publisher").Clear()
        MetroTile1.Visible = False
    End Sub
    Private Sub loadTableBuku()
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        mysqlconn.Open()
        SDA = New MySqlDataAdapter("select * from databuku  ", mysqlconn)
        SDA.Fill(dbDataSet, "databuku")

        DataGridView1.Rows.Clear()
        DataGridView1.Columns.Clear()
        DataGridView1.Columns.Add("Id", "id")
        DataGridView1.Columns.Add("title", "title")
        For i As Integer = 0 To dbDataSet.Tables("databuku").Rows.Count - 1
            DataGridView1.Rows.Add(
            dbDataSet.Tables("databuku").Rows(i)("id").ToString(),
            dbDataSet.Tables("databuku").Rows(i)("title").ToString()
            )
        Next
        dbDataSet.Tables("databuku").Clear()
        MetroTile1.Visible = False
    End Sub

    Private Sub loadTableAuthor()
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        mysqlconn.Open()
        SDA = New MySqlDataAdapter("select * from author  ", mysqlconn)
        SDA.Fill(dbDataSet, "author")

        DataGridView1.Rows.Clear()
        DataGridView1.Columns.Clear()
        DataGridView1.Columns.Add("Id", "id")
        DataGridView1.Columns.Add("nama", "nama")
        For i As Integer = 0 To dbDataSet.Tables("author").Rows.Count - 1
            DataGridView1.Rows.Add(
            dbDataSet.Tables("author").Rows(i)("id").ToString(),
            dbDataSet.Tables("author").Rows(i)("nama").ToString()
            )
        Next
        dbDataSet.Tables("author").Clear()
        MetroTile1.Visible = False
    End Sub
    Private Sub loadTableLibrary()
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        Try
            DataGridView1.Rows.Clear()
            DataGridView1.Columns.Clear()
            DataGridView1.Columns.Add("Title", "Title")
            mysqlconn.Open()
            Dim query As String
            query = "select * from tmplib.library where buyer='" & Mastermain.user & "'"
            SDA = New MySqlDataAdapter(query, mysqlconn)
            SDA.Fill(dbDataSet, "library")
            For i As Integer = 0 To dbDataSet.Tables("library").Rows.Count - 1
                DataGridView1.Rows.Add(
            dbDataSet.Tables("library").Rows(i)("title").ToString()
            )
            Next
            dbDataSet.Tables("library").Clear()
            mysqlconn.Close()
        Catch ex As MySqlException
        Finally
            mysqlconn.Dispose()
        End Try
        MetroTile1.Visible = True
    End Sub

    Private Sub loadTableFree()
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        Try
            DataGridView1.Rows.Clear()
            DataGridView1.Columns.Clear()
            DataGridView1.Columns.Add("Title", "Title")
            mysqlconn.Open()
            Dim query As String
            query = "select * from tmplib.databuku where price=0"
            SDA = New MySqlDataAdapter(query, mysqlconn)
            SDA.Fill(dbDataSet, "databuku")
            For i As Integer = 0 To dbDataSet.Tables("databuku").Rows.Count - 1
                DataGridView1.Rows.Add(
            dbDataSet.Tables("databuku").Rows(i)("title").ToString()
            )
            Next
            dbDataSet.Tables("databuku").Clear()
            mysqlconn.Close()
        Catch ex As MySqlException
        Finally
            mysqlconn.Dispose()
        End Try
        MetroTile1.Visible = True
    End Sub
    Public Sub cariBang(e As String)
        If e = "pegawai" Then
            Me.Text = "E-Library | Search Employee"
            loadTablePegawai()
            from = e
        ElseIf e = "buku" Then
            Me.Text = "E-Library | Search Book"
            loadTableBuku()
            from = e
        ElseIf e = "author" Then
            Me.Text = "E-Library | Search Author"
            loadTableAuthor()
            from = e
        ElseIf e = "library" Then
            Me.Text = "E-Library | My Library"
            loadTableLibrary()
            from = e
        ElseIf e = "publisher" Then
            Me.Text = "E-Library | Search Publisher"
            loadTablePublisher()
            from = e
        ElseIf e = "free" Then
            Me.Text = "E-Library | Free Book"
            loadTableFree()
            from = e
        End If
    End Sub

    Public Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        If from = "pegawai" Then
            MasterPegawai.transfer(DataGridView1.Item(0, DataGridView1.CurrentRow.Index).Value)
            Me.Close()
        ElseIf from = "author" Then
            MasterBuku.transfer(DataGridView1.Item(0, DataGridView1.CurrentRow.Index).Value, "author")
            Me.Close()
        ElseIf from = "publisher" Then
            MasterBuku.transfer(DataGridView1.Item(0, DataGridView1.CurrentRow.Index).Value, "publisher")
            Me.Close()
        ElseIf from = "buku" Then
            MasterBuku.transfer(DataGridView1.Item(0, DataGridView1.CurrentRow.Index).Value, "buku")
            Me.Close()
        ElseIf from = "library" Then

        Else

        End If
    End Sub
    Private Sub transferData(data As String)
        Dim tmp As String
        Me.Text = "E-Library | " & data
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        Try
            mysqlconn.Open()
            Dim query As String
            query = "select id from databuku where title='" & data & "'"
            mysqlcommand = mysqlconn.CreateCommand()
            mysqlcommand.CommandText = query
            tmp = mysqlcommand.ExecuteScalar()
            mysqlconn.Close()
        Catch ex As MySqlException
        Finally
            mysqlconn.Dispose()
        End Try
        System.Diagnostics.Process.Start(Application.StartupPath & "\data\" & tmp & ".pdf")
    End Sub

    Private Sub MetroTile1_Click(sender As Object, e As EventArgs) Handles MetroTile1.Click
        transferData(DataGridView1.Item(0, DataGridView1.CurrentRow.Index).Value)
    End Sub
End Class