﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class register
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.MetroRadioButton2 = New MetroFramework.Controls.MetroRadioButton()
        Me.MetroRadioButton1 = New MetroFramework.Controls.MetroRadioButton()
        Me.MetroTextBox2 = New MetroFramework.Controls.MetroTextBox()
        Me.HtmlLabel9 = New MetroFramework.Drawing.Html.HtmlLabel()
        Me.HtmlLabel2 = New MetroFramework.Drawing.Html.HtmlLabel()
        Me.HtmlLabel10 = New MetroFramework.Drawing.Html.HtmlLabel()
        Me.MetroTextBox8 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroTextBox7 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroComboBox1 = New MetroFramework.Controls.MetroComboBox()
        Me.MetroTextBox5 = New MetroFramework.Controls.MetroTextBox()
        Me.HtmlLabel1 = New MetroFramework.Drawing.Html.HtmlLabel()
        Me.HtmlLabel3 = New MetroFramework.Drawing.Html.HtmlLabel()
        Me.HtmlLabel4 = New MetroFramework.Drawing.Html.HtmlLabel()
        Me.MetroTextBox3 = New MetroFramework.Controls.MetroTextBox()
        Me.HtmlLabel5 = New MetroFramework.Drawing.Html.HtmlLabel()
        Me.MetroTextBox4 = New MetroFramework.Controls.MetroTextBox()
        Me.HtmlLabel6 = New MetroFramework.Drawing.Html.HtmlLabel()
        Me.HtmlLabel7 = New MetroFramework.Drawing.Html.HtmlLabel()
        Me.HtmlLabel8 = New MetroFramework.Drawing.Html.HtmlLabel()
        Me.MetroTextBox1 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroButton1 = New MetroFramework.Controls.MetroButton()
        Me.MetroPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.MetroRadioButton2)
        Me.MetroPanel1.Controls.Add(Me.MetroRadioButton1)
        Me.MetroPanel1.Controls.Add(Me.MetroTextBox2)
        Me.MetroPanel1.Controls.Add(Me.HtmlLabel9)
        Me.MetroPanel1.Controls.Add(Me.HtmlLabel2)
        Me.MetroPanel1.Controls.Add(Me.HtmlLabel10)
        Me.MetroPanel1.Controls.Add(Me.MetroTextBox8)
        Me.MetroPanel1.Controls.Add(Me.MetroTextBox7)
        Me.MetroPanel1.Controls.Add(Me.MetroComboBox1)
        Me.MetroPanel1.Controls.Add(Me.MetroTextBox5)
        Me.MetroPanel1.Controls.Add(Me.HtmlLabel1)
        Me.MetroPanel1.Controls.Add(Me.HtmlLabel3)
        Me.MetroPanel1.Controls.Add(Me.HtmlLabel4)
        Me.MetroPanel1.Controls.Add(Me.MetroTextBox3)
        Me.MetroPanel1.Controls.Add(Me.HtmlLabel5)
        Me.MetroPanel1.Controls.Add(Me.MetroTextBox4)
        Me.MetroPanel1.Controls.Add(Me.HtmlLabel6)
        Me.MetroPanel1.Controls.Add(Me.HtmlLabel7)
        Me.MetroPanel1.Controls.Add(Me.HtmlLabel8)
        Me.MetroPanel1.Controls.Add(Me.MetroTextBox1)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(23, 63)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(456, 388)
        Me.MetroPanel1.TabIndex = 19
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'MetroRadioButton2
        '
        Me.MetroRadioButton2.AutoSize = True
        Me.MetroRadioButton2.Location = New System.Drawing.Point(191, 156)
        Me.MetroRadioButton2.Name = "MetroRadioButton2"
        Me.MetroRadioButton2.Size = New System.Drawing.Size(61, 15)
        Me.MetroRadioButton2.TabIndex = 26
        Me.MetroRadioButton2.Text = "Female"
        Me.MetroRadioButton2.UseSelectable = True
        '
        'MetroRadioButton1
        '
        Me.MetroRadioButton1.AutoSize = True
        Me.MetroRadioButton1.Location = New System.Drawing.Point(136, 156)
        Me.MetroRadioButton1.Name = "MetroRadioButton1"
        Me.MetroRadioButton1.Size = New System.Drawing.Size(49, 15)
        Me.MetroRadioButton1.TabIndex = 25
        Me.MetroRadioButton1.Text = "Male"
        Me.MetroRadioButton1.UseSelectable = True
        '
        'MetroTextBox2
        '
        '
        '
        '
        Me.MetroTextBox2.CustomButton.Image = Nothing
        Me.MetroTextBox2.CustomButton.Location = New System.Drawing.Point(232, 2)
        Me.MetroTextBox2.CustomButton.Name = ""
        Me.MetroTextBox2.CustomButton.Size = New System.Drawing.Size(47, 47)
        Me.MetroTextBox2.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MetroTextBox2.CustomButton.TabIndex = 1
        Me.MetroTextBox2.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MetroTextBox2.CustomButton.UseSelectable = True
        Me.MetroTextBox2.CustomButton.Visible = False
        Me.MetroTextBox2.Lines = New String() {"MetroTextBox2"}
        Me.MetroTextBox2.Location = New System.Drawing.Point(136, 215)
        Me.MetroTextBox2.MaxLength = 32767
        Me.MetroTextBox2.Multiline = True
        Me.MetroTextBox2.Name = "MetroTextBox2"
        Me.MetroTextBox2.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MetroTextBox2.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MetroTextBox2.SelectedText = ""
        Me.MetroTextBox2.SelectionLength = 0
        Me.MetroTextBox2.SelectionStart = 0
        Me.MetroTextBox2.Size = New System.Drawing.Size(282, 52)
        Me.MetroTextBox2.TabIndex = 24
        Me.MetroTextBox2.Text = "MetroTextBox2"
        Me.MetroTextBox2.UseSelectable = True
        Me.MetroTextBox2.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MetroTextBox2.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'HtmlLabel9
        '
        Me.HtmlLabel9.AutoScroll = True
        Me.HtmlLabel9.AutoScrollMinSize = New System.Drawing.Size(51, 23)
        Me.HtmlLabel9.AutoSize = False
        Me.HtmlLabel9.BackColor = System.Drawing.SystemColors.Window
        Me.HtmlLabel9.Location = New System.Drawing.Point(13, 213)
        Me.HtmlLabel9.Name = "HtmlLabel9"
        Me.HtmlLabel9.Size = New System.Drawing.Size(98, 23)
        Me.HtmlLabel9.TabIndex = 23
        Me.HtmlLabel9.Text = "Address"
        '
        'HtmlLabel2
        '
        Me.HtmlLabel2.AutoScroll = True
        Me.HtmlLabel2.AutoScrollMinSize = New System.Drawing.Size(102, 23)
        Me.HtmlLabel2.AutoSize = False
        Me.HtmlLabel2.BackColor = System.Drawing.SystemColors.Window
        Me.HtmlLabel2.Location = New System.Drawing.Point(17, 262)
        Me.HtmlLabel2.Name = "HtmlLabel2"
        Me.HtmlLabel2.Size = New System.Drawing.Size(108, 23)
        Me.HtmlLabel2.TabIndex = 22
        Me.HtmlLabel2.Text = "Security Question "
        '
        'HtmlLabel10
        '
        Me.HtmlLabel10.AutoScroll = True
        Me.HtmlLabel10.AutoScrollMinSize = New System.Drawing.Size(59, 23)
        Me.HtmlLabel10.AutoSize = False
        Me.HtmlLabel10.BackColor = System.Drawing.SystemColors.Window
        Me.HtmlLabel10.Location = New System.Drawing.Point(13, 56)
        Me.HtmlLabel10.Name = "HtmlLabel10"
        Me.HtmlLabel10.Size = New System.Drawing.Size(75, 23)
        Me.HtmlLabel10.TabIndex = 20
        Me.HtmlLabel10.Text = "Student Id"
        '
        'MetroTextBox8
        '
        '
        '
        '
        Me.MetroTextBox8.CustomButton.Image = Nothing
        Me.MetroTextBox8.CustomButton.Location = New System.Drawing.Point(190, 1)
        Me.MetroTextBox8.CustomButton.Name = ""
        Me.MetroTextBox8.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.MetroTextBox8.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MetroTextBox8.CustomButton.TabIndex = 1
        Me.MetroTextBox8.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MetroTextBox8.CustomButton.UseSelectable = True
        Me.MetroTextBox8.CustomButton.Visible = False
        Me.MetroTextBox8.Lines = New String() {"MetroTextBox8"}
        Me.MetroTextBox8.Location = New System.Drawing.Point(134, 55)
        Me.MetroTextBox8.MaxLength = 32767
        Me.MetroTextBox8.Name = "MetroTextBox8"
        Me.MetroTextBox8.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MetroTextBox8.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MetroTextBox8.SelectedText = ""
        Me.MetroTextBox8.SelectionLength = 0
        Me.MetroTextBox8.SelectionStart = 0
        Me.MetroTextBox8.Size = New System.Drawing.Size(212, 23)
        Me.MetroTextBox8.TabIndex = 21
        Me.MetroTextBox8.Text = "MetroTextBox8"
        Me.MetroTextBox8.UseSelectable = True
        Me.MetroTextBox8.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MetroTextBox8.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroTextBox7
        '
        '
        '
        '
        Me.MetroTextBox7.CustomButton.Image = Nothing
        Me.MetroTextBox7.CustomButton.Location = New System.Drawing.Point(260, 1)
        Me.MetroTextBox7.CustomButton.Name = ""
        Me.MetroTextBox7.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.MetroTextBox7.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MetroTextBox7.CustomButton.TabIndex = 1
        Me.MetroTextBox7.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MetroTextBox7.CustomButton.UseSelectable = True
        Me.MetroTextBox7.CustomButton.Visible = False
        Me.MetroTextBox7.Lines = New String() {"MetroTextBox7"}
        Me.MetroTextBox7.Location = New System.Drawing.Point(136, 326)
        Me.MetroTextBox7.MaxLength = 32767
        Me.MetroTextBox7.Name = "MetroTextBox7"
        Me.MetroTextBox7.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MetroTextBox7.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MetroTextBox7.SelectedText = ""
        Me.MetroTextBox7.SelectionLength = 0
        Me.MetroTextBox7.SelectionStart = 0
        Me.MetroTextBox7.Size = New System.Drawing.Size(282, 23)
        Me.MetroTextBox7.TabIndex = 17
        Me.MetroTextBox7.Text = "MetroTextBox7"
        Me.MetroTextBox7.UseSelectable = True
        Me.MetroTextBox7.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MetroTextBox7.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroComboBox1
        '
        Me.MetroComboBox1.FormattingEnabled = True
        Me.MetroComboBox1.ItemHeight = 23
        Me.MetroComboBox1.Items.AddRange(New Object() {"Siapakah nama anak anjing anda ?", "Siapakah teman  masa kecil anda ?", "Siapakah nama paman ibu anda ?", "Masukan tanggal lahir ibu anda!", "Siapakah nama paman dari ayah anda ?"})
        Me.MetroComboBox1.Location = New System.Drawing.Point(136, 285)
        Me.MetroComboBox1.Name = "MetroComboBox1"
        Me.MetroComboBox1.Size = New System.Drawing.Size(282, 29)
        Me.MetroComboBox1.TabIndex = 16
        Me.MetroComboBox1.UseSelectable = True
        '
        'MetroTextBox5
        '
        '
        '
        '
        Me.MetroTextBox5.CustomButton.Image = Nothing
        Me.MetroTextBox5.CustomButton.Location = New System.Drawing.Point(188, 1)
        Me.MetroTextBox5.CustomButton.Name = ""
        Me.MetroTextBox5.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.MetroTextBox5.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MetroTextBox5.CustomButton.TabIndex = 1
        Me.MetroTextBox5.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MetroTextBox5.CustomButton.UseSelectable = True
        Me.MetroTextBox5.CustomButton.Visible = False
        Me.MetroTextBox5.Lines = New String() {"MetroTextBox5"}
        Me.MetroTextBox5.Location = New System.Drawing.Point(136, 186)
        Me.MetroTextBox5.MaxLength = 32767
        Me.MetroTextBox5.Name = "MetroTextBox5"
        Me.MetroTextBox5.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MetroTextBox5.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MetroTextBox5.SelectedText = ""
        Me.MetroTextBox5.SelectionLength = 0
        Me.MetroTextBox5.SelectionStart = 0
        Me.MetroTextBox5.Size = New System.Drawing.Size(210, 23)
        Me.MetroTextBox5.TabIndex = 15
        Me.MetroTextBox5.Text = "MetroTextBox5"
        Me.MetroTextBox5.UseSelectable = True
        Me.MetroTextBox5.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MetroTextBox5.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'HtmlLabel1
        '
        Me.HtmlLabel1.AutoScroll = True
        Me.HtmlLabel1.AutoScrollMinSize = New System.Drawing.Size(39, 23)
        Me.HtmlLabel1.AutoSize = False
        Me.HtmlLabel1.BackColor = System.Drawing.SystemColors.Window
        Me.HtmlLabel1.Location = New System.Drawing.Point(13, 27)
        Me.HtmlLabel1.Name = "HtmlLabel1"
        Me.HtmlLabel1.Size = New System.Drawing.Size(75, 23)
        Me.HtmlLabel1.TabIndex = 0
        Me.HtmlLabel1.Text = "Name"
        '
        'HtmlLabel3
        '
        Me.HtmlLabel3.AutoScroll = True
        Me.HtmlLabel3.AutoScrollMinSize = New System.Drawing.Size(59, 23)
        Me.HtmlLabel3.AutoSize = False
        Me.HtmlLabel3.BackColor = System.Drawing.SystemColors.Window
        Me.HtmlLabel3.Location = New System.Drawing.Point(13, 86)
        Me.HtmlLabel3.Name = "HtmlLabel3"
        Me.HtmlLabel3.Size = New System.Drawing.Size(75, 23)
        Me.HtmlLabel3.TabIndex = 2
        Me.HtmlLabel3.Text = "Password"
        '
        'HtmlLabel4
        '
        Me.HtmlLabel4.AutoScroll = True
        Me.HtmlLabel4.AutoScrollMinSize = New System.Drawing.Size(101, 23)
        Me.HtmlLabel4.AutoSize = False
        Me.HtmlLabel4.BackColor = System.Drawing.SystemColors.Window
        Me.HtmlLabel4.Location = New System.Drawing.Point(13, 116)
        Me.HtmlLabel4.Name = "HtmlLabel4"
        Me.HtmlLabel4.Size = New System.Drawing.Size(112, 23)
        Me.HtmlLabel4.TabIndex = 3
        Me.HtmlLabel4.Text = "Confirm Password"
        '
        'MetroTextBox3
        '
        '
        '
        '
        Me.MetroTextBox3.CustomButton.Image = Nothing
        Me.MetroTextBox3.CustomButton.Location = New System.Drawing.Point(190, 1)
        Me.MetroTextBox3.CustomButton.Name = ""
        Me.MetroTextBox3.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.MetroTextBox3.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MetroTextBox3.CustomButton.TabIndex = 1
        Me.MetroTextBox3.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MetroTextBox3.CustomButton.UseSelectable = True
        Me.MetroTextBox3.CustomButton.Visible = False
        Me.MetroTextBox3.Lines = New String() {"MetroTextBox3"}
        Me.MetroTextBox3.Location = New System.Drawing.Point(134, 113)
        Me.MetroTextBox3.MaxLength = 32767
        Me.MetroTextBox3.Name = "MetroTextBox3"
        Me.MetroTextBox3.PasswordChar = Global.Microsoft.VisualBasic.ChrW(8226)
        Me.MetroTextBox3.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MetroTextBox3.SelectedText = ""
        Me.MetroTextBox3.SelectionLength = 0
        Me.MetroTextBox3.SelectionStart = 0
        Me.MetroTextBox3.Size = New System.Drawing.Size(212, 23)
        Me.MetroTextBox3.TabIndex = 12
        Me.MetroTextBox3.Text = "MetroTextBox3"
        Me.MetroTextBox3.UseSelectable = True
        Me.MetroTextBox3.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MetroTextBox3.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'HtmlLabel5
        '
        Me.HtmlLabel5.AutoScroll = True
        Me.HtmlLabel5.AutoScrollMinSize = New System.Drawing.Size(47, 23)
        Me.HtmlLabel5.AutoSize = False
        Me.HtmlLabel5.BackColor = System.Drawing.SystemColors.Window
        Me.HtmlLabel5.Location = New System.Drawing.Point(13, 149)
        Me.HtmlLabel5.Name = "HtmlLabel5"
        Me.HtmlLabel5.Size = New System.Drawing.Size(75, 23)
        Me.HtmlLabel5.TabIndex = 4
        Me.HtmlLabel5.Text = "Gender"
        '
        'MetroTextBox4
        '
        '
        '
        '
        Me.MetroTextBox4.CustomButton.Image = Nothing
        Me.MetroTextBox4.CustomButton.Location = New System.Drawing.Point(190, 1)
        Me.MetroTextBox4.CustomButton.Name = ""
        Me.MetroTextBox4.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.MetroTextBox4.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MetroTextBox4.CustomButton.TabIndex = 1
        Me.MetroTextBox4.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MetroTextBox4.CustomButton.UseSelectable = True
        Me.MetroTextBox4.CustomButton.Visible = False
        Me.MetroTextBox4.Lines = New String() {"MetroTextBox4"}
        Me.MetroTextBox4.Location = New System.Drawing.Point(134, 84)
        Me.MetroTextBox4.MaxLength = 32767
        Me.MetroTextBox4.Name = "MetroTextBox4"
        Me.MetroTextBox4.PasswordChar = Global.Microsoft.VisualBasic.ChrW(8226)
        Me.MetroTextBox4.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MetroTextBox4.SelectedText = ""
        Me.MetroTextBox4.SelectionLength = 0
        Me.MetroTextBox4.SelectionStart = 0
        Me.MetroTextBox4.Size = New System.Drawing.Size(212, 23)
        Me.MetroTextBox4.TabIndex = 11
        Me.MetroTextBox4.Text = "MetroTextBox4"
        Me.MetroTextBox4.UseSelectable = True
        Me.MetroTextBox4.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MetroTextBox4.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'HtmlLabel6
        '
        Me.HtmlLabel6.AutoScroll = True
        Me.HtmlLabel6.AutoScrollMinSize = New System.Drawing.Size(77, 23)
        Me.HtmlLabel6.AutoSize = False
        Me.HtmlLabel6.BackColor = System.Drawing.SystemColors.Window
        Me.HtmlLabel6.Location = New System.Drawing.Point(13, 184)
        Me.HtmlLabel6.Name = "HtmlLabel6"
        Me.HtmlLabel6.Size = New System.Drawing.Size(98, 23)
        Me.HtmlLabel6.TabIndex = 5
        Me.HtmlLabel6.Text = "Mobile Phone"
        '
        'HtmlLabel7
        '
        Me.HtmlLabel7.AutoScroll = True
        Me.HtmlLabel7.AutoScrollMinSize = New System.Drawing.Size(58, 23)
        Me.HtmlLabel7.AutoSize = False
        Me.HtmlLabel7.BackColor = System.Drawing.SystemColors.Window
        Me.HtmlLabel7.Location = New System.Drawing.Point(14, 291)
        Me.HtmlLabel7.Name = "HtmlLabel7"
        Me.HtmlLabel7.Size = New System.Drawing.Size(75, 23)
        Me.HtmlLabel7.TabIndex = 6
        Me.HtmlLabel7.Text = "Question "
        '
        'HtmlLabel8
        '
        Me.HtmlLabel8.AutoScroll = True
        Me.HtmlLabel8.AutoScrollMinSize = New System.Drawing.Size(47, 23)
        Me.HtmlLabel8.AutoSize = False
        Me.HtmlLabel8.BackColor = System.Drawing.SystemColors.Window
        Me.HtmlLabel8.Location = New System.Drawing.Point(14, 326)
        Me.HtmlLabel8.Name = "HtmlLabel8"
        Me.HtmlLabel8.Size = New System.Drawing.Size(75, 23)
        Me.HtmlLabel8.TabIndex = 7
        Me.HtmlLabel8.Text = "Answer"
        '
        'MetroTextBox1
        '
        '
        '
        '
        Me.MetroTextBox1.CustomButton.Image = Nothing
        Me.MetroTextBox1.CustomButton.Location = New System.Drawing.Point(190, 1)
        Me.MetroTextBox1.CustomButton.Name = ""
        Me.MetroTextBox1.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.MetroTextBox1.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MetroTextBox1.CustomButton.TabIndex = 1
        Me.MetroTextBox1.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MetroTextBox1.CustomButton.UseSelectable = True
        Me.MetroTextBox1.CustomButton.Visible = False
        Me.MetroTextBox1.Lines = New String() {"MetroTextBox1"}
        Me.MetroTextBox1.Location = New System.Drawing.Point(134, 26)
        Me.MetroTextBox1.MaxLength = 32767
        Me.MetroTextBox1.Name = "MetroTextBox1"
        Me.MetroTextBox1.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MetroTextBox1.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MetroTextBox1.SelectedText = ""
        Me.MetroTextBox1.SelectionLength = 0
        Me.MetroTextBox1.SelectionStart = 0
        Me.MetroTextBox1.Size = New System.Drawing.Size(212, 23)
        Me.MetroTextBox1.TabIndex = 8
        Me.MetroTextBox1.Text = "MetroTextBox1"
        Me.MetroTextBox1.UseSelectable = True
        Me.MetroTextBox1.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MetroTextBox1.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroButton1
        '
        Me.MetroButton1.Location = New System.Drawing.Point(211, 457)
        Me.MetroButton1.Name = "MetroButton1"
        Me.MetroButton1.Size = New System.Drawing.Size(75, 23)
        Me.MetroButton1.TabIndex = 18
        Me.MetroButton1.Text = "Register"
        Me.MetroButton1.UseSelectable = True
        '
        'register
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(502, 496)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Controls.Add(Me.MetroButton1)
        Me.Name = "register"
        Me.Resizable = False
        Me.Text = "E-Library | Register"
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents HtmlLabel10 As MetroFramework.Drawing.Html.HtmlLabel
    Friend WithEvents MetroTextBox8 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroTextBox7 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroButton1 As MetroFramework.Controls.MetroButton
    Friend WithEvents MetroComboBox1 As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroTextBox5 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents HtmlLabel1 As MetroFramework.Drawing.Html.HtmlLabel
    Friend WithEvents HtmlLabel3 As MetroFramework.Drawing.Html.HtmlLabel
    Friend WithEvents HtmlLabel4 As MetroFramework.Drawing.Html.HtmlLabel
    Friend WithEvents MetroTextBox3 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents HtmlLabel5 As MetroFramework.Drawing.Html.HtmlLabel
    Friend WithEvents MetroTextBox4 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents HtmlLabel6 As MetroFramework.Drawing.Html.HtmlLabel
    Friend WithEvents HtmlLabel7 As MetroFramework.Drawing.Html.HtmlLabel
    Friend WithEvents HtmlLabel8 As MetroFramework.Drawing.Html.HtmlLabel
    Friend WithEvents MetroTextBox1 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents HtmlLabel2 As MetroFramework.Drawing.Html.HtmlLabel
    Friend WithEvents MetroTextBox2 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents HtmlLabel9 As MetroFramework.Drawing.Html.HtmlLabel
    Friend WithEvents MetroRadioButton2 As MetroFramework.Controls.MetroRadioButton
    Friend WithEvents MetroRadioButton1 As MetroFramework.Controls.MetroRadioButton
End Class
