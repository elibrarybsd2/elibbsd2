﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainMenu
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FILEToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GANTIPASSWORDToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MASTERToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PEGAWAIToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MAHASISWAToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BOOKToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OPTIONSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ABOUTUSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HELPToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.LabelTanggal = New System.Windows.Forms.Label()
        Me.LabelJam = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.ButtonLogout = New System.Windows.Forms.Button()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.SystemColors.HotTrack
        Me.MenuStrip1.Font = New System.Drawing.Font("Segoe UI Semibold", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FILEToolStripMenuItem, Me.MASTERToolStripMenuItem, Me.OPTIONSToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(28, 75)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1103, 31)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FILEToolStripMenuItem
        '
        Me.FILEToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GANTIPASSWORDToolStripMenuItem})
        Me.FILEToolStripMenuItem.Name = "FILEToolStripMenuItem"
        Me.FILEToolStripMenuItem.Size = New System.Drawing.Size(53, 27)
        Me.FILEToolStripMenuItem.Text = "FILE"
        '
        'GANTIPASSWORDToolStripMenuItem
        '
        Me.GANTIPASSWORDToolStripMenuItem.Name = "GANTIPASSWORDToolStripMenuItem"
        Me.GANTIPASSWORDToolStripMenuItem.Size = New System.Drawing.Size(231, 28)
        Me.GANTIPASSWORDToolStripMenuItem.Text = "GANTI PASSWORD"
        '
        'MASTERToolStripMenuItem
        '
        Me.MASTERToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PEGAWAIToolStripMenuItem, Me.MAHASISWAToolStripMenuItem, Me.BOOKToolStripMenuItem})
        Me.MASTERToolStripMenuItem.Name = "MASTERToolStripMenuItem"
        Me.MASTERToolStripMenuItem.Size = New System.Drawing.Size(87, 27)
        Me.MASTERToolStripMenuItem.Text = "MASTER"
        '
        'PEGAWAIToolStripMenuItem
        '
        Me.PEGAWAIToolStripMenuItem.Name = "PEGAWAIToolStripMenuItem"
        Me.PEGAWAIToolStripMenuItem.Size = New System.Drawing.Size(186, 28)
        Me.PEGAWAIToolStripMenuItem.Text = "PEGAWAI"
        '
        'MAHASISWAToolStripMenuItem
        '
        Me.MAHASISWAToolStripMenuItem.Name = "MAHASISWAToolStripMenuItem"
        Me.MAHASISWAToolStripMenuItem.Size = New System.Drawing.Size(186, 28)
        Me.MAHASISWAToolStripMenuItem.Text = "MAHASISWA"
        '
        'BOOKToolStripMenuItem
        '
        Me.BOOKToolStripMenuItem.Name = "BOOKToolStripMenuItem"
        Me.BOOKToolStripMenuItem.Size = New System.Drawing.Size(186, 28)
        Me.BOOKToolStripMenuItem.Text = "BOOK"
        '
        'OPTIONSToolStripMenuItem
        '
        Me.OPTIONSToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ABOUTUSToolStripMenuItem, Me.HELPToolStripMenuItem})
        Me.OPTIONSToolStripMenuItem.Name = "OPTIONSToolStripMenuItem"
        Me.OPTIONSToolStripMenuItem.Size = New System.Drawing.Size(94, 27)
        Me.OPTIONSToolStripMenuItem.Text = "OPTIONS"
        '
        'ABOUTUSToolStripMenuItem
        '
        Me.ABOUTUSToolStripMenuItem.Name = "ABOUTUSToolStripMenuItem"
        Me.ABOUTUSToolStripMenuItem.Size = New System.Drawing.Size(167, 28)
        Me.ABOUTUSToolStripMenuItem.Text = "ABOUT US"
        '
        'HELPToolStripMenuItem
        '
        Me.HELPToolStripMenuItem.Name = "HELPToolStripMenuItem"
        Me.HELPToolStripMenuItem.Size = New System.Drawing.Size(167, 28)
        Me.HELPToolStripMenuItem.Text = "HELP"
        '
        'MetroLabel1
        '
        Me.MetroLabel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(810, 17)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(48, 20)
        Me.MetroLabel1.TabIndex = 1
        Me.MetroLabel1.Text = "Hello ,"
        '
        'MetroLabel2
        '
        Me.MetroLabel2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.Location = New System.Drawing.Point(864, 17)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(49, 20)
        Me.MetroLabel2.TabIndex = 1
        Me.MetroLabel2.Text = "Admin"
        '
        'LabelTanggal
        '
        Me.LabelTanggal.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.LabelTanggal.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelTanggal.Location = New System.Drawing.Point(268, 17)
        Me.LabelTanggal.Name = "LabelTanggal"
        Me.LabelTanggal.Size = New System.Drawing.Size(274, 33)
        Me.LabelTanggal.TabIndex = 19
        Me.LabelTanggal.Text = "Tanggal"
        '
        'LabelJam
        '
        Me.LabelJam.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.LabelJam.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelJam.Location = New System.Drawing.Point(548, 17)
        Me.LabelJam.Name = "LabelJam"
        Me.LabelJam.Size = New System.Drawing.Size(135, 33)
        Me.LabelJam.TabIndex = 19
        Me.LabelJam.Text = "Jam"
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        '
        'ButtonLogout
        '
        Me.ButtonLogout.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButtonLogout.BackColor = System.Drawing.SystemColors.HotTrack
        Me.ButtonLogout.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonLogout.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonLogout.Location = New System.Drawing.Point(937, 11)
        Me.ButtonLogout.Name = "ButtonLogout"
        Me.ButtonLogout.Size = New System.Drawing.Size(107, 39)
        Me.ButtonLogout.TabIndex = 132
        Me.ButtonLogout.Text = "LOGOUT"
        Me.ButtonLogout.UseVisualStyleBackColor = False
        '
        'MainMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(11.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1159, 668)
        Me.Controls.Add(Me.ButtonLogout)
        Me.Controls.Add(Me.LabelJam)
        Me.Controls.Add(Me.LabelTanggal)
        Me.Controls.Add(Me.MetroLabel2)
        Me.Controls.Add(Me.MetroLabel1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "MainMenu"
        Me.Padding = New System.Windows.Forms.Padding(28, 75, 28, 25)
        Me.Text = "Menu Utama"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents FILEToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MASTERToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents OPTIONSToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ABOUTUSToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents HELPToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents PEGAWAIToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MAHASISWAToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BOOKToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GANTIPASSWORDToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents LabelTanggal As Label
    Friend WithEvents LabelJam As Label
    Friend WithEvents Timer1 As Timer
    Friend WithEvents ButtonLogout As Button
End Class
