﻿Imports System.IO
Imports MySql.Data.MySqlClient
Public Class MasterBuku
    Private Sub MasterBuku_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        refreshData()
    End Sub
    Public Sub transfer(t As String, f As String)
        If f = "author" Then
            MetroLabel35.Text = t
            mysqlconn = New MySqlConnection
            mysqlconn.ConnectionString = Mastermain.koneksi
            Try
                mysqlconn.Open()
                Dim query As String
                query = "select * from tmplib.author where  id = '" & t & "'"
                mysqlcommand = New MySqlCommand(query, mysqlconn)
                mysqlreader = mysqlcommand.ExecuteReader
                While mysqlreader.Read
                    MetroLabel36.Text = mysqlreader.GetString("nama")
                    MetroLabel37.Text = mysqlreader.GetString("gender")
                    MetroTextBox24.Text = mysqlreader.GetString("address")
                    MetroTextBox23.Text = mysqlreader.GetString("phone")
                End While
                mysqlconn.Close()
            Catch ex As MySqlException
            Finally
                mysqlconn.Dispose()
            End Try
        ElseIf f = "publisher" Then
            MetroLabel28.Text = t
            mysqlconn = New MySqlConnection
            mysqlconn.ConnectionString = Mastermain.koneksi
            Try
                mysqlconn.Open()
                Dim query As String
                query = "select * from tmplib.publisher where  id = '" & t & "'"
                mysqlcommand = New MySqlCommand(query, mysqlconn)
                mysqlreader = mysqlcommand.ExecuteReader
                While mysqlreader.Read
                    MetroTextBox18.Text = mysqlreader.GetString("nama")
                    MetroTextBox15.Text = mysqlreader.GetString("owner")
                    MetroTextBox17.Text = mysqlreader.GetString("address")
                    MetroTextBox16.Text = mysqlreader.GetString("phone")
                End While
                mysqlconn.Close()
            Catch ex As MySqlException
            Finally
                mysqlconn.Dispose()
            End Try
        ElseIf f = "buku" Then
            mysqlconn = New MySqlConnection
            mysqlconn.ConnectionString = Mastermain.koneksi
            Try
                mysqlconn.Open()
                Dim query As String
                query = "select * from tmplib.databuku where  id = '" & t & "'"
                mysqlcommand = New MySqlCommand(query, mysqlconn)
                mysqlreader = mysqlcommand.ExecuteReader
                While mysqlreader.Read
                End While
                mysqlconn.Close()
            Catch ex As MySqlException
            Finally
                mysqlconn.Dispose()
            End Try
        End If
    End Sub
    Dim mysqlconn As MySqlConnection
    Dim mysqlcommand As MySqlCommand
    Dim mysqlreader As MySqlDataReader
    Private Sub refreshData()
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        MetroTextBox8.Text = ""
        MetroTextBox7.Text = ""
        MetroTextBox6.Text = ""
        MetroTextBox4.Text = ""
        MetroTextBox1.Text = ""
        Try
            mysqlconn.Open()
            Dim query As String
            query = "select AUTO_INCREMENT from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA = 'tmplib' and TABLE_NAME = 'databuku'"
            mysqlcommand = mysqlconn.CreateCommand()
            mysqlcommand.CommandText = query
            MetroTextBox8.Text = mysqlcommand.ExecuteScalar()
            mysqlconn.Close()
        Catch ex As MySqlException
        Finally
            mysqlconn.Dispose()
        End Try

        MetroComboBox2.Items.Clear()
        Try
            mysqlconn.Open()
            Dim query As String
            query = "select * from tmplib.publisher"
            mysqlcommand = New MySqlCommand(query, mysqlconn)
            mysqlreader = mysqlcommand.ExecuteReader
            While mysqlreader.Read
                MetroComboBox2.Items.Add(mysqlreader.GetString("nama"))
            End While
            mysqlconn.Close()
        Catch ex As MySqlException
        Finally
            mysqlconn.Dispose()
        End Try
        MetroComboBox1.Items.Clear()
        Try
            mysqlconn.Open()
            Dim query As String
            query = "select * from tmplib.author"
            mysqlcommand = New MySqlCommand(query, mysqlconn)
            mysqlreader = mysqlcommand.ExecuteReader
            While mysqlreader.Read
                MetroComboBox1.Items.Add(mysqlreader.GetString("nama"))
            End While
            mysqlconn.Close()
        Catch ex As MySqlException
        Finally
            mysqlconn.Dispose()
        End Try
        '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        MetroRadioButton1.Checked = False
        MetroRadioButton2.Checked = False
        MetroTextBox21.Text = ""
        MetroTextBox22.Text = ""
        MetroTextBox19.Text = ""
        MetroTextBox4.Text = ""
        MetroTextBox1.Text = ""
        Try
            mysqlconn.Open()
            Dim query As String
            query = "select AUTO_INCREMENT from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA = 'tmplib' and TABLE_NAME = 'author'"
            mysqlcommand = mysqlconn.CreateCommand()
            mysqlcommand.CommandText = query
            MetroTextBox22.Text = mysqlcommand.ExecuteScalar()
            mysqlconn.Close()
        Catch ex As MySqlException
        Finally
            mysqlconn.Dispose()
        End Try
        '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        MetroTextBox10.Text = ""
        MetroTextBox11.Text = ""
        MetroTextBox12.Text = ""
        MetroTextBox13.Text = ""
        Try
            mysqlconn.Open()
            Dim query As String
            query = "select AUTO_INCREMENT from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA = 'tmplib' and TABLE_NAME = 'publisher'"
            mysqlcommand = mysqlconn.CreateCommand()
            mysqlcommand.CommandText = query
            MetroTextBox14.Text = mysqlcommand.ExecuteScalar()
            mysqlconn.Close()
        Catch ex As MySqlException
        Finally
            mysqlconn.Dispose()
        End Try
    End Sub
    Private Sub MetroTile2_MouseEnter(sender As Object, e As EventArgs) Handles MetroTile2.MouseEnter
        MetroTile2.Style = 5
    End Sub

    Private Sub MetroPanel1_MouseEnter(sender As Object, e As EventArgs) Handles MetroPanel1.MouseEnter
        MetroTile2.Style = 6
    End Sub



    Private Sub MasterBuku_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        adminArea.Aktif()
    End Sub

    Private Sub MetroTile5_Click(sender As Object, e As EventArgs)
        FormCari.cariBang("buku")
        FormCari.Show()
    End Sub

    Private Sub MetroTile14_Click(sender As Object, e As EventArgs) Handles MetroTile14.Click
        FormCari.cariBang("publisher")
        FormCari.Show()
    End Sub

    Private Sub MetroTile12_Click(sender As Object, e As EventArgs) Handles MetroTile12.Click
        FormCari.cariBang("author")
        FormCari.Show()
    End Sub

    Private Sub MetroTile10_Click(sender As Object, e As EventArgs) Handles MetroTile10.Click
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        Try
            Dim status As String

            If MetroRadioButton1.Checked Then
                status = "Male"
            ElseIf MetroRadioButton2.Checked Then
                status = "Female"
            End If
            mysqlconn.Open()
            Dim query As String
            query = "insert into tmplib.author(id, nama, gender, address, phone) values(0,'" & MetroTextBox21.Text & "','" & status & "','" & MetroTextBox20.Text & "','" & MetroTextBox19.Text & "')"
            mysqlcommand = New MySqlCommand(query, mysqlconn)
            mysqlreader = mysqlcommand.ExecuteReader
            mysqlconn.Close()
            MessageBox.Show("Data Added")
            refreshData()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            mysqlconn.Dispose()
        End Try
    End Sub

    Private Sub MetroTile6_Click(sender As Object, e As EventArgs) Handles MetroTile6.Click
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        Try
            mysqlconn.Open()
            Dim query As String
            query = "insert into tmplib.publisher(id, nama, owner, address, phone) values(0,'" & MetroTextBox13.Text & "','" & MetroTextBox12.Text & "','" & MetroTextBox11.Text & "','" & MetroTextBox10.Text & "')"
            mysqlcommand = New MySqlCommand(query, mysqlconn)
            mysqlreader = mysqlcommand.ExecuteReader
            mysqlconn.Close()
            MessageBox.Show("Data Added")
            refreshData()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            mysqlconn.Dispose()
        End Try
    End Sub

    Private Sub MetroTile13_Click(sender As Object, e As EventArgs) Handles MetroTile13.Click
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        Dim mysqlreader As MySqlDataReader
        Try
            mysqlconn.Open()
            Dim query As String
            query = "update tmplib.author set address = '" & MetroTextBox24.Text & "', phone ='" & MetroTextBox23.Text & "' where id = '" & MetroLabel35.Text & "'"
            mysqlcommand = New MySqlCommand(query, mysqlconn)
            mysqlreader = mysqlcommand.ExecuteReader
            mysqlconn.Close()
            MessageBox.Show("Data Updated")
        Catch ex As MySqlException
        Finally
            mysqlconn.Dispose()
        End Try
    End Sub

    Private Sub MetroTile8_Click(sender As Object, e As EventArgs) Handles MetroTile8.Click
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        Dim mysqlreader As MySqlDataReader
        Try
            mysqlconn.Open()
            Dim query As String
            query = "update tmplib.publisher set nama = '" & MetroTextBox18.Text & "', owner ='" & MetroTextBox15.Text & "' , address ='" & MetroTextBox17.Text & "', phone ='" & MetroTextBox16.Text & "' where id = '" & MetroLabel28.Text & "'"
            mysqlcommand = New MySqlCommand(query, mysqlconn)
            mysqlreader = mysqlcommand.ExecuteReader
            mysqlconn.Close()
            MessageBox.Show("Data Updated")
        Catch ex As MySqlException
        Finally
            mysqlconn.Dispose()
        End Try
    End Sub

    Function ImageToByte(ByVal pbImg As System.Windows.Forms.PictureBox) As Byte()
        If pbImg Is Nothing Then
            Return Nothing
        End If
        Dim ms As New System.IO.MemoryStream()
        pbImg.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg)
        Return ms.ToArray()
    End Function
    Public Function ByteToImage(ByVal filefoto As Byte()) As Image
        Dim pictureBytes As New MemoryStream(filefoto)
        Return Image.FromStream(pictureBytes)
    End Function

    Private Sub MetroTile2_Click(sender As Object, e As EventArgs) Handles MetroTile2.Click
        Dim res = MessageBox.Show("Are you sure confirm this update ?", "Warning!", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
        If res = Windows.Forms.DialogResult.Yes Then
            mysqlconn = New MySqlConnection
            mysqlconn.ConnectionString = Mastermain.koneksi
            Try
                mysqlconn.Open()
                Dim query As String
                query = "insert into tmplib.databuku(id, title, author, publisher, year,category,description,price,download,epick,featured) values(0,'" & MetroTextBox1.Text & "','" & MetroComboBox1.SelectedItem & "','" & MetroComboBox2.SelectedItem & "','" & MetroTextBox4.Text & "','" & MetroComboBox3.SelectedItem & "','" & MetroTextBox6.Text & "','" & MetroTextBox7.Text & "',0,false,false)"
                mysqlcommand = New MySqlCommand(query, mysqlconn)
                mysqlreader = mysqlcommand.ExecuteReader
                mysqlconn.Close()
                MessageBox.Show("Data Added")
            Catch ex As MySqlException
                MessageBox.Show(ex.Message)
            Finally
                mysqlconn.Dispose()
            End Try
            '...............................................................................................................

            strFilename = System.IO.Path.GetFileName(OpenFileDialog1.FileName.ToString())
            strBasepath = Application.StartupPath & "\data"
            If Directory.Exists(strBasepath) = False Then
                Call Directory.CreateDirectory(strBasepath)
            End If
            Call PictureBox1.Image.Save(strBasepath & "\" & strFilename, System.Drawing.Imaging.ImageFormat.Jpeg)
            My.Computer.FileSystem.RenameFile(strBasepath + "\" + strFilename, MetroTextBox8.Text + ".jpg")
            '................................................................................................................
            PictureBox1.Image = Nothing
            '................................................................................................................
            strFilenamePDF = OpenFileDialog2.FileName.ToString()
            strBasepathPDF = Application.StartupPath & "\pdf"
            If Directory.Exists(strBasepathPDF) = False Then
                Call Directory.CreateDirectory(strBasepathPDF)
            End If
            My.Computer.FileSystem.CopyFile(strFilenamePDF, strBasepathPDF & "\" & MetroTextBox8.Text + ".pdf")
            ''
            refreshData()
        End If
    End Sub
    Dim strBasepath As String
    Dim strFilename As String
    Dim strFilenamePDF As String
    Dim strBasepathPDF As String

    Private Sub OpenFileDialog1_FileOk(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles OpenFileDialog1.FileOk
        If OpenFileDialog1.CheckFileExists = True Then
            PictureBox1.ImageLocation = OpenFileDialog1.FileName.ToString
            PictureBox1.SizeMode = PictureBoxSizeMode.StretchImage
        Else
            MsgBox("Please Select a Image")
        End If

    End Sub

    Private Sub MetroTile3_Click_1(sender As Object, e As EventArgs) Handles MetroTile3.Click
        OpenFileDialog1.Filter = "only images|*.jpg"
        OpenFileDialog1.Title = "Select a file"
        Dim s As String = Application.StartupPath & "\data\"
        OpenFileDialog1.InitialDirectory = s
        OpenFileDialog1.ShowDialog()
    End Sub

    Private Sub MetroTile16_Click(sender As Object, e As EventArgs) Handles MetroTile16.Click
        OpenFileDialog2.Filter = "only pdf files|*.pdf"
        OpenFileDialog2.Title = "Select a file"
        Dim s As String = Application.StartupPath & "\pdf\"
        OpenFileDialog2.InitialDirectory = s
        OpenFileDialog2.ShowDialog()
    End Sub

    Private Sub OpenFileDialog2_FileOk(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles OpenFileDialog2.FileOk
        If OpenFileDialog1.CheckFileExists = True Then
            MsgBox("Data Added")
        Else
            MsgBox("Please Select a data")
        End If
    End Sub

    Private Sub MetroTile1_Click(sender As Object, e As EventArgs)

    End Sub
End Class