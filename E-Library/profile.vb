﻿Imports MySql.Data.MySqlClient
Public Class profile
    Dim mysqlconn As MySqlConnection
    Dim mysqlcommand As MySqlCommand
    Dim SDA As New MySqlDataAdapter
    Dim mysqlreader As MySqlDataReader
    Dim dbDataSet As New DataSet
    Private Sub profile_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.WindowState = FormWindowState.Maximized
        MetroLabel1.Text = Mastermain.nama
        MetroLabel5.Text = Mastermain.poin
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        Dim i As Int32 = 0
        Try
            mysqlconn.Open()
            Dim query As String
            query = "select * from tmplib.library where buyer='" & Mastermain.user & "'"
            mysqlcommand = New MySqlCommand(query, mysqlconn)
            mysqlreader = mysqlcommand.ExecuteReader

            While mysqlreader.Read
                i = i + 1
            End While
            MetroLabel2.Text = i
            mysqlconn.Close()
        Catch ex As MySqlException
        Finally
            mysqlconn.Dispose()
        End Try
        loadRecentDownload()
    End Sub

    Private Sub MetroTile1_Click(sender As Object, e As EventArgs) Handles MetroTile1.Click
        tambahPoin.Show()
    End Sub
    Public Sub loadRecentDownload()
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        Try
            DataGridView1.Rows.Clear()
            DataGridView1.Columns.Clear()
            DataGridView1.Columns.Add("Title", "Title")
            DataGridView1.Columns.Add("Purchase Date", "Purchase Date")
            mysqlconn.Open()
            Dim query As String
            query = "select * from tmplib.library where buyer='" & Mastermain.user & "'"
            SDA = New MySqlDataAdapter(query, mysqlconn)
            SDA.Fill(dbDataSet, "library")
            For i As Integer = 0 To dbDataSet.Tables("library").Rows.Count - 1
                DataGridView1.Rows.Add(
            dbDataSet.Tables("library").Rows(i)("title").ToString(),
            dbDataSet.Tables("library").Rows(i)("tanggal").ToString()
            )
            Next
            dbDataSet.Tables("library").Clear()
            mysqlconn.Close()
        Catch ex As MySqlException
        Finally
            mysqlconn.Dispose()
        End Try
    End Sub
    Public Sub resetPoin()
        MetroLabel5.Text = Mastermain.poin
    End Sub

    Private Sub MetroTile5_Click(sender As Object, e As EventArgs) Handles MetroTile5.Click
        FormCari.cariBang("library")
        FormCari.Show()
    End Sub

    Private Sub MetroTile2_Click(sender As Object, e As EventArgs) Handles MetroTile2.Click
        FormCari.cariBang("free")
        FormCari.Show()
    End Sub
End Class