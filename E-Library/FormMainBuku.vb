﻿Imports System.Globalization
Imports MySql.Data.MySqlClient
Public Class FormMainBuku
    Dim mysqlconn As MySqlConnection
    Dim mysqlcommand As MySqlCommand
    Dim SDA As New MySqlDataAdapter
    Dim dbDataSet As New DataSet
    Dim state As String
    Private Sub FormMainBukuvb_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.WindowState = FormWindowState.Maximized
    End Sub
    'Mastermain.koneksi
    Public Sub loadAll()
        state = "a"
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        mysqlconn.Open()
        SDA = New MySqlDataAdapter("select * from databuku  ", mysqlconn)
        SDA.Fill(dbDataSet, "databuku")
        Dim price As String
        DataGridView1.Rows.Clear()
        DataGridView1.Columns.Clear()
        DataGridView1.Columns.Add("title", "title")
        DataGridView1.Columns.Add("author", "author")
        DataGridView1.Columns.Add("publisher", "publisher")
        DataGridView1.Columns.Add("category", "category")
        DataGridView1.Columns.Add("price", "price")
        DataGridView1.Columns.Add("downloaded", "downloaded")
        For i As Integer = 0 To dbDataSet.Tables("databuku").Rows.Count - 1
            If dbDataSet.Tables("databuku").Rows(i)("price") = 0 Then
                price = "Free"
            Else
                price = dbDataSet.Tables("databuku").Rows(i)("price").ToString() + " Points"
            End If
            DataGridView1.Rows.Add(
            dbDataSet.Tables("databuku").Rows(i)("title").ToString(),
            dbDataSet.Tables("databuku").Rows(i)("author").ToString(),
            dbDataSet.Tables("databuku").Rows(i)("publisher").ToString(),
            dbDataSet.Tables("databuku").Rows(i)("category").ToString(),
            price,
            dbDataSet.Tables("databuku").Rows(i)("download").ToString() + " Times"
            )
        Next
        dbDataSet.Tables("databuku").Clear()

    End Sub
    Public Sub loadFeatured()
        state = "f"
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        mysqlconn.Open()
        SDA = New MySqlDataAdapter("select * from databuku  ", mysqlconn)
        SDA.Fill(dbDataSet, "databuku")
        Dim price As String
        DataGridView1.Rows.Clear()
        DataGridView1.Columns.Clear()
        DataGridView1.Columns.Add("title", "title")
        DataGridView1.Columns.Add("author", "author")
        DataGridView1.Columns.Add("publisher", "publisher")
        DataGridView1.Columns.Add("category", "category")
        DataGridView1.Columns.Add("price", "price")
        DataGridView1.Columns.Add("downloaded", "downloaded")
        For i As Integer = 0 To dbDataSet.Tables("databuku").Rows.Count - 1
            If dbDataSet.Tables("databuku").Rows(i)("featured") Then
                If dbDataSet.Tables("databuku").Rows(i)("price") = 0 Then
                    price = "Free"
                Else
                    price = dbDataSet.Tables("databuku").Rows(i)("price").ToString() + " Points"
                End If
                DataGridView1.Rows.Add(
            dbDataSet.Tables("databuku").Rows(i)("title").ToString(),
            dbDataSet.Tables("databuku").Rows(i)("author").ToString(),
            dbDataSet.Tables("databuku").Rows(i)("publisher").ToString(),
            dbDataSet.Tables("databuku").Rows(i)("category").ToString(),
            price,
            dbDataSet.Tables("databuku").Rows(i)("download").ToString() + " Times"
            )
            End If
        Next
        dbDataSet.Tables("databuku").Clear()

    End Sub

    Public Sub loadEpick()
        state = "e"
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        mysqlconn.Open()
        SDA = New MySqlDataAdapter("select * from databuku  ", mysqlconn)
        SDA.Fill(dbDataSet, "databuku")
        Dim price As String
        DataGridView1.Rows.Clear()
        DataGridView1.Columns.Clear()
        DataGridView1.Columns.Add("title", "title")
        DataGridView1.Columns.Add("author", "author")
        DataGridView1.Columns.Add("publisher", "publisher")
        DataGridView1.Columns.Add("category", "category")
        DataGridView1.Columns.Add("price", "price")
        DataGridView1.Columns.Add("downloaded", "downloaded")
        For i As Integer = 0 To dbDataSet.Tables("databuku").Rows.Count - 1
            If dbDataSet.Tables("databuku").Rows(i)("epick") Then
                If dbDataSet.Tables("databuku").Rows(i)("price") = 0 Then
                    price = "Free"
                Else
                    price = dbDataSet.Tables("databuku").Rows(i)("price").ToString() + " Points"
                End If
                DataGridView1.Rows.Add(
            dbDataSet.Tables("databuku").Rows(i)("title").ToString(),
            dbDataSet.Tables("databuku").Rows(i)("author").ToString(),
            dbDataSet.Tables("databuku").Rows(i)("publisher").ToString(),
            dbDataSet.Tables("databuku").Rows(i)("category").ToString(),
            price,
            dbDataSet.Tables("databuku").Rows(i)("download").ToString() + " Times"
            )
            End If
        Next
        dbDataSet.Tables("databuku").Clear()

    End Sub
    Public Sub LoadNew()
        Try
            state = "t"
            mysqlconn = New MySqlConnection
            mysqlconn.ConnectionString = Mastermain.koneksi
            mysqlconn.Open()
            SDA = New MySqlDataAdapter("select * from databuku order by year desc", mysqlconn)
            SDA.Fill(dbDataSet, "databuku")
            Dim c As Int32 = 0
            Dim price As String
            DataGridView1.Rows.Clear()
            DataGridView1.Columns.Clear()
            DataGridView1.Columns.Add("title", "title")
            DataGridView1.Columns.Add("author", "author")
            DataGridView1.Columns.Add("publisher", "publisher")
            DataGridView1.Columns.Add("category", "category")
            DataGridView1.Columns.Add("price", "price")
            DataGridView1.Columns.Add("downloaded", "downloaded")
            For i As Integer = 0 To dbDataSet.Tables("databuku").Rows.Count - 1
                If c <= 10 Then
                    If dbDataSet.Tables("databuku").Rows(i)("price") = 0 Then
                        price = "Free"
                    Else
                        price = dbDataSet.Tables("databuku").Rows(i)("price").ToString() + " Points"
                    End If
                    DataGridView1.Rows.Add(
            dbDataSet.Tables("databuku").Rows(i)("title").ToString(),
            dbDataSet.Tables("databuku").Rows(i)("author").ToString(),
            dbDataSet.Tables("databuku").Rows(i)("publisher").ToString(),
            dbDataSet.Tables("databuku").Rows(i)("category").ToString(),
            price,
            dbDataSet.Tables("databuku").Rows(i)("download").ToString() + " Times"
            )
                    c = c + 1
                End If
            Next
            dbDataSet.Tables("databuku").Clear()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try


    End Sub
    Public Sub loadLast()
        Try
            state = "t"
            mysqlconn = New MySqlConnection
            mysqlconn.ConnectionString = Mastermain.koneksi
            mysqlconn.Open()
            SDA = New MySqlDataAdapter("select * from databuku order by id desc", mysqlconn)
            SDA.Fill(dbDataSet, "databuku")
            Dim c As Int32 = 0
            Dim price As String
            DataGridView1.Rows.Clear()
            DataGridView1.Columns.Clear()
            DataGridView1.Columns.Add("title", "title")
            DataGridView1.Columns.Add("author", "author")
            DataGridView1.Columns.Add("publisher", "publisher")
            DataGridView1.Columns.Add("category", "category")
            DataGridView1.Columns.Add("price", "price")
            DataGridView1.Columns.Add("downloaded", "downloaded")
            For i As Integer = 0 To dbDataSet.Tables("databuku").Rows.Count - 1
                If c <= 10 Then
                    If dbDataSet.Tables("databuku").Rows(i)("price") = 0 Then
                        price = "Free"
                    Else
                        price = dbDataSet.Tables("databuku").Rows(i)("price").ToString() + " Points"
                    End If
                    DataGridView1.Rows.Add(
            dbDataSet.Tables("databuku").Rows(i)("title").ToString(),
            dbDataSet.Tables("databuku").Rows(i)("author").ToString(),
            dbDataSet.Tables("databuku").Rows(i)("publisher").ToString(),
            dbDataSet.Tables("databuku").Rows(i)("category").ToString(),
            price,
            dbDataSet.Tables("databuku").Rows(i)("download").ToString() + " Times"
            )
                    c = c + 1
                End If
            Next
            dbDataSet.Tables("databuku").Clear()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub
    Public Sub loadTopDwnl()
        Try
            state = "t"
            mysqlconn = New MySqlConnection
            mysqlconn.ConnectionString = Mastermain.koneksi
            mysqlconn.Open()
            SDA = New MySqlDataAdapter("select * from databuku order by download desc", mysqlconn)
            SDA.Fill(dbDataSet, "databuku")
            Dim c As Int32 = 0
            Dim price As String
            DataGridView1.Rows.Clear()
            DataGridView1.Columns.Clear()
            DataGridView1.Columns.Add("title", "title")
            DataGridView1.Columns.Add("author", "author")
            DataGridView1.Columns.Add("publisher", "publisher")
            DataGridView1.Columns.Add("category", "category")
            DataGridView1.Columns.Add("price", "price")
            DataGridView1.Columns.Add("downloaded", "downloaded")
            For i As Integer = 0 To dbDataSet.Tables("databuku").Rows.Count - 1
                If c <= 10 Then
                    If dbDataSet.Tables("databuku").Rows(i)("price") = 0 Then
                        price = "Free"
                    Else
                        price = dbDataSet.Tables("databuku").Rows(i)("price").ToString() + " Points"
                    End If
                    DataGridView1.Rows.Add(
            dbDataSet.Tables("databuku").Rows(i)("title").ToString(),
            dbDataSet.Tables("databuku").Rows(i)("author").ToString(),
            dbDataSet.Tables("databuku").Rows(i)("publisher").ToString(),
            dbDataSet.Tables("databuku").Rows(i)("category").ToString(),
            price,
            dbDataSet.Tables("databuku").Rows(i)("download").ToString() + " Times"
            )
                    c = c + 1
                End If
            Next
            dbDataSet.Tables("databuku").Clear()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        FormDetailBuku.cariBukuBos(DataGridView1.Item(0, DataGridView1.CurrentRow.Index).Value)
        FormDetailBuku.Show()
    End Sub

    Private Sub MetroTextBox1_Click(sender As Object, e As EventArgs) Handles MetroTextBox1.Click

    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        If state = "a" Then
            loadAll()
        ElseIf state = "f" Then
            loadFeatured()
        ElseIf state = "e" Then
            loadEpick()
        ElseIf state = "t" Then
            loadTopDwnl()
        End If
    End Sub

    Private Sub MetroTextBox1_TextChanged(sender As Object, e As EventArgs) Handles MetroTextBox1.TextChanged
        Dim culture As New CultureInfo("es-ES", False)

        For Each row In DataGridView1.Rows
            If culture.CompareInfo.IndexOf(row.Cells(0).value, sender.Text, CompareOptions.IgnoreCase) >= 0 Then
                row.Selected = True
                Exit For
            End If
        Next
    End Sub
End Class