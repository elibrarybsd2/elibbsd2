﻿Imports MySql.Data.MySqlClient
Public Class MasterPegawai

    Private Sub MasterPegawai_Load(sender As Object, e As EventArgs) Handles MyBase.MouseEnter
        refreshData()
    End Sub
    Dim mysqlconn As MySqlConnection
    Dim mysqlcommand As MySqlCommand
    Dim mysqlreader As MySqlDataReader
    Private Sub refreshData()
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        MetroRadioButton1.Checked = False
        MetroRadioButton2.Checked = False
        MetroTextBox8.Text = ""
        MetroTextBox7.Text = ""
        MetroTextBox6.Text = ""
        MetroTextBox3.Text = ""
        MetroTextBox4.Text = ""
        MetroTextBox5.Text = ""
        Try
            mysqlconn.Open()
            Dim query As String
            query = "select AUTO_INCREMENT from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA = 'tmplib' and TABLE_NAME = 'pegawai'"
            mysqlcommand = mysqlconn.CreateCommand()
            mysqlcommand.CommandText = query
            MetroTextBox8.Text = mysqlcommand.ExecuteScalar()

            mysqlconn.Close()
        Catch ex As MySqlException

        Finally
            mysqlconn.Dispose()
        End Try
    End Sub

    Private Sub MetroTile2_MouseEnter(sender As Object, e As EventArgs) Handles MetroTile2.MouseEnter
        MetroTile2.Style = 5
    End Sub

    Private Sub MetroPanel1_MouseEnter(sender As Object, e As EventArgs) Handles MetroPanel1.MouseEnter
        MetroTile2.Style = 6
    End Sub

    Private Sub MetroTile1_MouseEnter(sender As Object, e As EventArgs) Handles MetroTile1.MouseEnter
        MetroTile1.Style = 5
    End Sub
    Private Sub MetroTile5_MouseEnter(sender As Object, e As EventArgs) Handles MetroTile5.MouseEnter
        MetroTile5.Style = 5
    End Sub

    Private Sub MetroPanel2_MouseEnter(sender As Object, e As EventArgs) Handles MetroPanel2.MouseEnter
        MetroTile1.Style = 6
        MetroTile5.Style = 6
    End Sub

    Public Sub transfer(e As String)
        MetroLabel15.Text = e
        mysqlconn = New MySqlConnection
        mysqlconn.ConnectionString = Mastermain.koneksi
        Try
            mysqlconn.Open()
            Dim query As String
            query = "select * from tmplib.pegawai where  id = '" & e & "'"
            mysqlcommand = New MySqlCommand(query, mysqlconn)
            mysqlreader = mysqlcommand.ExecuteReader
            While mysqlreader.Read
                MetroLabel14.Text = mysqlreader.GetString("nama")
                MetroLabel13.Text = mysqlreader.GetString("gender")
                MetroTextBox2.Text = mysqlreader.GetString("address")
                MetroTextBox1.Text = mysqlreader.GetString("phone")
            End While
            mysqlconn.Close()
        Catch ex As MySqlException
        Finally
            mysqlconn.Dispose()
        End Try
    End Sub

    Private Sub MetroTile5_Click(sender As Object, e As EventArgs) Handles MetroTile5.Click
        FormCari.cariBang("pegawai")
        FormCari.Show()
    End Sub

    Private Sub MasterPegawai_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        adminArea.Aktif()
    End Sub

    Private Sub MetroTile2_Click(sender As Object, e As EventArgs) Handles MetroTile2.Click
        If Not MetroTextBox8.Text = "" And MetroTextBox4.Text = MetroTextBox5.Text Then
            mysqlconn = New MySqlConnection
            mysqlconn.ConnectionString = Mastermain.koneksi
            Try
                Dim status As String

                If MetroRadioButton1.Checked Then
                    status = "Male"
                ElseIf MetroRadioButton2.Checked Then
                    status = "Female"
                End If
                mysqlconn.Open()
                Dim query As String
                query = "insert into tmplib.pegawai(id, nama, gender, address, phone, password) values(0,'" & MetroTextBox7.Text & "','" & status & "','" & MetroTextBox6.Text & "','" & MetroTextBox3.Text & "','" & MetroTextBox5.Text & "')"
                mysqlcommand = New MySqlCommand(query, mysqlconn)
                mysqlreader = mysqlcommand.ExecuteReader
                mysqlconn.Close()
                MessageBox.Show("Data Added")
                refreshData()
            Catch ex As MySqlException
                MessageBox.Show(ex.Message)
            Finally
                mysqlconn.Dispose()
            End Try
        Else
            MessageBox.Show("Data Invalid")
        End If

    End Sub

    Private Sub MetroTile1_Click(sender As Object, e As EventArgs) Handles MetroTile1.Click
        Dim res = MessageBox.Show("Are you sure confirm this changes ?", "Warning!", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
        If res = Windows.Forms.DialogResult.Yes Then
            mysqlconn = New MySqlConnection
            mysqlconn.ConnectionString = Mastermain.koneksi
            Dim mysqlreader As MySqlDataReader
            Try
                mysqlconn.Open()
                Dim query As String
                query = "update tmplib.pegawai set address = '" & MetroTextBox2.Text & "', phone ='" & MetroTextBox1.Text & "' where id = '" & MetroLabel15.Text & "'"
                mysqlcommand = New MySqlCommand(query, mysqlconn)
                mysqlreader = mysqlcommand.ExecuteReader
                mysqlconn.Close()
                MessageBox.Show("Data Updated")
            Catch ex As MySqlException
            Finally
                mysqlconn.Dispose()
            End Try
        Else
            MessageBox.Show("Data not Changed")
        End If
    End Sub

End Class