-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 17 Jun 2016 pada 16.43
-- Versi Server: 10.1.10-MariaDB
-- PHP Version: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tmplib`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `author`
--

CREATE TABLE `author` (
  `id` bigint(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `address` varchar(500) NOT NULL,
  `phone` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `author`
--

INSERT INTO `author` (`id`, `nama`, `gender`, `address`, `phone`) VALUES
(1, 'ReVa', 'Female', 'Manila', '08656796580'),
(2, 'Dan Brown', 'Male', 'USA', '085413256'),
(3, 'Erlangga', 'Male', 'Surabaya', '0857345687'),
(4, 'Rick Riordan', 'Male', 'USA', '0987654345');

-- --------------------------------------------------------

--
-- Struktur dari tabel `beli`
--

CREATE TABLE `beli` (
  `id` bigint(20) NOT NULL,
  `poin` int(11) NOT NULL,
  `harga` bigint(20) NOT NULL,
  `total` bigint(20) NOT NULL,
  `ccode` varchar(5) NOT NULL,
  `admin` varchar(20) NOT NULL,
  `date` varchar(50) NOT NULL,
  `c` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `beli`
--

INSERT INTO `beli` (`id`, `poin`, `harga`, `total`, `ccode`, `admin`, `date`, `c`) VALUES
(10, 20, 2000, 40000, '8m4FC', '1', 'Friday, 10 - June - 2016', 1),
(11, 100, 2000, 200000, 'UBH6p', '1', 'Friday, 10 - June - 2016', 1),
(12, 30, 2000, 60000, 'zBAxO', '1', 'Friday, 10 - June - 2016', 1),
(13, 50, 2000, 100000, '7l9ts', '1', 'Friday, 10 - June - 2016', 1),
(14, 50, 2000, 100000, '69mtK', '1', 'Friday, 10 - June - 2016', 1),
(15, 50, 2000, 100000, '8khtK', '1', 'Friday, 10 - June - 2016', 1),
(16, 50, 2000, 100000, 'fRdx0', '1', 'Friday, 10 - June - 2016', 1),
(17, 200, 2000, 400000, 'WzxDV', '1', 'Friday, 10 - June - 2016', 1),
(18, 100, 2000, 200000, 'BRfEE', '1', 'Friday, 10 - June - 2016', 1),
(19, 100, 2000, 200000, 'Qg57c', '', '', 0),
(20, 500, 2000, 1000000, 'IY0y9', '1', 'Friday, 17 - June - 2016', 1),
(21, 10, 2000, 20000, 'qNedq', '', '', 0),
(22, 10, 2000, 20000, '5e3h8', '1', 'Friday, 17 - June - 2016', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `buku`
--
-- sedang digunakan(#1932 - Table 'tmplib.buku' doesn't exist in engine)
-- Galat pembacaan data: (#1932 - Table 'tmplib.buku' doesn't exist in engine)

-- --------------------------------------------------------

--
-- Struktur dari tabel `databuku`
--

CREATE TABLE `databuku` (
  `id` bigint(20) NOT NULL,
  `title` varchar(50) NOT NULL,
  `author` varchar(50) NOT NULL,
  `publisher` varchar(50) NOT NULL,
  `year` varchar(50) NOT NULL,
  `category` varchar(50) NOT NULL,
  `description` varchar(500) NOT NULL,
  `price` int(11) NOT NULL,
  `download` int(11) NOT NULL,
  `epick` tinyint(1) NOT NULL,
  `featured` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `databuku`
--

INSERT INTO `databuku` (`id`, `title`, `author`, `publisher`, `year`, `category`, `description`, `price`, `download`, `epick`, `featured`) VALUES
(2, 'Angry Birds Tutorial', 'ReVa', 'Banana', '2016', 'Fun & Games', 'Tutorial Angry Birds untuk mendapatkan 10 bintang!!', 50, 2, 1, 0),
(3, 'Overwatch Guide', 'ReVa', 'Banana', '2016', 'Fun & Games', 'Overwatch Guide for beginner', 25, 1, 0, 1),
(4, 'How to be Dragonborn', 'ReVa', 'Diansyah', '1990', 'Fun & Games', 'Best Game become best Book', 900, 0, 0, 0),
(5, 'Inferno', 'Dan Brown', 'Banana', '2013', 'Fiction', 'The Da Vinci Code', 50, 1, 0, 0),
(6, 'Percy Jackson the Olympians', 'Rick Riordan', 'Diansyah', '2010', 'Fiction', 'Novel Terbaik Sepanjang Sejarah ', 300, 1, 0, 0),
(7, 'Mikasa Ackerman', 'ReVa', 'Diansyah', '2013', 'Performing Arts', 'Mikasa Ackerman book', 0, 0, 0, 0),
(8, 'java', 'Erlangga', 'Banana', '2016', 'Philosophy', 'ini deskripsi buku', 10, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `komen`
--

CREATE TABLE `komen` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `komentar` varchar(500) NOT NULL,
  `idbuku` int(11) NOT NULL,
  `tanggal` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `komen`
--

INSERT INTO `komen` (`id`, `nama`, `komentar`, `idbuku`, `tanggal`) VALUES
(1, 'Rheza Valerian', 'Good job! bukunya sangat tidak membantu!', 2, 'Friday, 03 - June - '),
(2, 'Rismoyo Suryo', 'not bad', 2, 'Friday, 10 - June - '),
(3, 'Rismoyo Suryo', 'best!', 2, 'Friday, 10 - June - '),
(4, 'Rismoyo Suryo', 'jelek', 3, 'Friday, 10 - June - '),
(5, 'Rismoyo Suryo', 'good', 3, 'Friday, 10 - June - '),
(6, 'Rheza Valerian', 'Yey!', 3, 'Thursday, 16 - June '),
(7, 'Rismoyo Suryo', 'Nice Book.. Recommended Gan   !!!!!', 6, 'Friday, 17 - June - '),
(8, 'Rismoyo Suryo', 'Recomended banget gan ....!!!!', 5, 'Friday, 17 - June - '),
(9, 'Rismoyo Suryo', 'bagus', 7, 'Friday, 17 - June - '),
(10, 'Rismoyo Suryo', 'good', 2, 'Friday, 17 - June - '),
(11, 'Rismoyo Suryo', 'good', 5, 'Friday, 17 - June - ');

-- --------------------------------------------------------

--
-- Struktur dari tabel `library`
--

CREATE TABLE `library` (
  `id` int(11) NOT NULL,
  `idbuku` varchar(10) NOT NULL,
  `title` varchar(30) NOT NULL,
  `buyer` varchar(10) NOT NULL,
  `tanggal` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `library`
--

INSERT INTO `library` (`id`, `idbuku`, `title`, `buyer`, `tanggal`) VALUES
(2, '2', 'Angry Birds Tutorial', '214310249', 'Thursday, 02 - June - 2016'),
(3, '2', 'Angry Birds Tutorial', '214310250', 'Friday, 10 - June - 2016'),
(4, '3', 'Overwatch Guide', '214310250', 'Friday, 10 - June - 2016'),
(5, '6', 'Percy Jackson the Olympians', '214310250', 'Friday, 17 - June - 2016'),
(6, '5', 'Inferno', '214310250', 'Friday, 17 - June - 2016');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE `pegawai` (
  `id` bigint(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `address` varchar(500) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`id`, `nama`, `gender`, `address`, `phone`, `password`) VALUES
(1, 'Rheza Valerian', 'Male', 'Steam', '0899999999', '1234'),
(2, 'Yoman', 'Female', 'Jamaica', '087779997', '1234'),
(3, 'hendra', 'Male', 'dupak', '0812', '1234');

-- --------------------------------------------------------

--
-- Struktur dari tabel `poin`
--

CREATE TABLE `poin` (
  `id` int(11) NOT NULL,
  `tgl` varchar(20) NOT NULL,
  `harga` int(11) NOT NULL,
  `admin` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `poin`
--

INSERT INTO `poin` (`id`, `tgl`, `harga`, `admin`) VALUES
(1, 'Friday, 20 - May - 2', 2000, 'Rheza Valerian'),
(2, 'Friday, 20 - May - 2', 3000, 'Rheza Valerian'),
(3, 'Friday, 20 - May - 2', 2000, 'Rheza Valerian'),
(4, 'Friday, 17 - June - ', 4000, 'Rheza Valerian');

-- --------------------------------------------------------

--
-- Struktur dari tabel `publisher`
--

CREATE TABLE `publisher` (
  `id` bigint(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `owner` varchar(50) NOT NULL,
  `address` varchar(500) NOT NULL,
  `phone` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `publisher`
--

INSERT INTO `publisher` (`id`, `nama`, `owner`, `address`, `phone`) VALUES
(1, 'Diansyah', 'Babang', 'Ngagel', '08200100'),
(2, 'Banana', 'Gru', 'Lazy Town', '0866655443');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rating`
--

CREATE TABLE `rating` (
  `id` varchar(10) NOT NULL,
  `rating` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `rating`
--

INSERT INTO `rating` (`id`, `rating`) VALUES
('2', 3),
('2', 5),
('3', 1),
('3', 3),
('3', 5),
('6', 5),
('5', 5),
('7', 3),
('2', 3),
('5', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa`
--

CREATE TABLE `siswa` (
  `id` bigint(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  `poin` int(11) NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `address` varchar(50) NOT NULL,
  `hp` varchar(20) NOT NULL,
  `pertanyaan` varchar(50) NOT NULL,
  `jawaban` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `siswa`
--

INSERT INTO `siswa` (`id`, `nama`, `password`, `poin`, `gender`, `address`, `hp`, `pertanyaan`, `jawaban`) VALUES
(99, 'RVB', '99', 0, 0, '99', '99', 'Siapakah teman  masa kecil anda ?', '99'),
(214310249, 'Rheza Valerian', '1234', 200, 0, 'Jamaica', '08999xxx', '', ''),
(214310250, 'Rismoyo Suryo', '1234', 485, 0, 'Jamaica', '08999320579', 'Siapakah nama anak anjing anda ?', 'puppy');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` int(50) NOT NULL,
  `gender` varchar(25) NOT NULL,
  `Address` varchar(150) NOT NULL,
  `hp` varchar(13) NOT NULL,
  `id_pertanyaan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `name`, `email`, `gender`, `Address`, `hp`, `id_pertanyaan`) VALUES
(3, 'a', 'b', 'saya', 0, 'Male', 'assss', '089564423', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `author`
--
ALTER TABLE `author`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `beli`
--
ALTER TABLE `beli`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `databuku`
--
ALTER TABLE `databuku`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `komen`
--
ALTER TABLE `komen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `library`
--
ALTER TABLE `library`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `poin`
--
ALTER TABLE `poin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `publisher`
--
ALTER TABLE `publisher`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `author`
--
ALTER TABLE `author`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `beli`
--
ALTER TABLE `beli`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `databuku`
--
ALTER TABLE `databuku`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `komen`
--
ALTER TABLE `komen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `library`
--
ALTER TABLE `library`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `poin`
--
ALTER TABLE `poin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `publisher`
--
ALTER TABLE `publisher`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
